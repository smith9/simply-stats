package simplystats.config

/**
 * Created: 19/04/12
 * @author david
 */
class Band {
    String css
    String colour
    BigDecimal threshold
}
