package simplystats.config

import groovy.json.JsonSlurper
import org.apache.commons.collections.iterators.ReverseListIterator

/**
 * Created: 19/04/12
 * @author david
 */
class BandList implements Bands {
    String lowestDescription = ""
    List<Band> bands = []

    public static BandList load(File jsonFile) {
        BandList bands = new BandList()
        Map json = new JsonSlurper().parseText(jsonFile.text) as Map
        json.bands.each { Map band ->
            bands.add(band.css, band.colour, band.threshold)
        }
        return bands
    }

    public String generateCss() {
        String NEWLINE = System.getProperty("line.separator")
        StringBuilder builder = new StringBuilder()
        bands.each { Band band ->
            if(band.css && band.colour) {
                builder.append(".").append(band.css).
                        append(" { background-color: ").append(band.colour).append("; }").append(NEWLINE)
            }
        }
        return builder.toString()
    }

    public void add(String css, String colour, BigDecimal threshold) {
        bands << new Band(css: css, threshold: threshold, colour: colour)
    }
    public Band getBand(BigDecimal value) {
        Band band = null;
        ReverseListIterator iterator = new ReverseListIterator(bands)
        while(iterator.hasNext()) {
            Band current = iterator.next()
            if(value >= current.threshold) {
                band = current
                break
            }
        }
        return band
    }
}
