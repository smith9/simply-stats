package simplystats.config

/**
 * User: david
 * Date: 21/05/13
 * Time: 8:47 AM
 */
class Config {
    BandList bands

    Config(File configDir) {
        this.configDir = configDir
    }

    File configDir
    BandList getBands() {
        if(!bands) {
            bands = BandList.load(new File(configDir, "bands.json"))
        }
        return bands
    }
}
