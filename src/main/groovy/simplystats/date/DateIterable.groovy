package simplystats.date

import org.joda.time.DateTime
import org.joda.time.Interval

/**
 * User: david
 * Date: 27/04/13
 * Time: 9:59 AM
 */
class DateIterable implements Iterable<Interval> {
    Interval interval
    Plus incrementorType
    int increment
    IteratorEnd ending = IteratorEnd.END_AT_RANGE_END

    DateIterable(Interval interval, Plus incrementorType, int increment = 1, IteratorEnd ending = IteratorEnd.END_AT_RANGE_END) {
        this.interval = interval
        this.incrementorType = incrementorType
        this.increment = increment
        this.ending = ending
    }
    DateIterable(DateTime start, DateTime end, Plus incrementorType, int increment = 1, IteratorEnd ending = IteratorEnd.END_AT_RANGE_END) {
        this(new Interval(start, end), incrementorType, increment, ending)
    }

    Iterator<Interval> iterator() {
        return new DateIterator(interval, incrementorType, increment, ending)
    }
}
