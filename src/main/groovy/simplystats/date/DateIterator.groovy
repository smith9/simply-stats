package simplystats.date

import org.joda.time.DateTime
import org.joda.time.Interval

/**
 * User: david
 * Date: 27/04/13
 * Time: 9:44 AM
 */
class DateIterator implements Iterator<Interval>{
    Interval interval
    DateTime start
    Closure incrementor
    IteratorEnd ending

    DateIterator(Interval interval, Plus incrementorType, int increment = 1, IteratorEnd ending = IteratorEnd.END_AT_RANGE_END) {
        this.interval = interval
        this.start  = interval.start
        this.incrementor = incrementorType.getIncrementor(increment)
        this.ending = ending
    }

    boolean hasNext() {
        if(ending == IteratorEnd.END_ON_OR_AFTER_RANGE_END || ending == IteratorEnd.END_AT_RANGE_END) {
            return start.isBefore(interval.end)
        } else {
            return !incrementor(start).isAfter(interval.end)
        }
    }

    Interval next() {
        DateTime end = incrementor(start)
        if(ending == IteratorEnd.END_AT_RANGE_END && end > interval.end) {
            end = interval.end
        }
        Interval next = new Interval(start, end)
        start = end
        return next
    }

    void remove() {
        throw new UnsupportedOperationException("not supported")
    }
}
