package simplystats.date

/**
 * User: david
 * Date: 1/05/13
 * Time: 12:59 PM
 */
public enum IteratorEnd {
    END_AT_RANGE_END,
    END_ON_OR_AFTER_RANGE_END,
    END_ON_OR_BEFORE_RANGE_END
}