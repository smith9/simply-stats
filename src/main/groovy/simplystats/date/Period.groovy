package simplystats.date

import org.joda.time.DateTime
import org.joda.time.Days
import org.joda.time.format.DateTimeFormatter

/**
 * User: david
 * Date: 25/04/13
 * Time: 10:59 PM
 */
enum Period {
    Minutes1(1),
    Minutes5(5),
    Minutes10(10),
    Minutes15(15),
    Minutes20(20),
    Minutes30(30),
    Hours1(60),
    Days1(24*60)

    Period(int minutes) {
        this.minutes = minutes
    }

    int minutes

    public DateTime floor(DateTime readingTime) {
        DateTime floor
        switch (this) {
            case Minutes1:
                floor = readingTime.minuteOfHour().roundFloorCopy()
                break
            case Minutes5:
            case Minutes10:
            case Minutes15:
            case Minutes20:
            case Minutes30:
                int mod = readingTime.getMinuteOfHour() % minutes
                floor = readingTime.minuteOfHour().roundFloorCopy().minusMinutes(mod)
                break
            case Hours1:
                floor = readingTime.hourOfDay().roundFloorCopy()
                break
        }
        return floor
    }

    public DateTime ceiling(DateTime readingTime) {
        DateTime floor = floor(readingTime)
        return floor.plusMinutes(minutes)
    }
}
