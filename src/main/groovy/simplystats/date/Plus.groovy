package simplystats.date

import org.joda.time.DateTime

/**
 * User: david
 * Date: 27/04/13
 * Time: 10:12 AM
 */
enum Plus {
    MILLIS({ int increment, DateTime time -> time.plusMillis(increment)}),
    SECONDS({ int increment, DateTime time -> time.plusSeconds(increment)}),
    MINUTES({ int increment, DateTime time -> time.plusMinutes(increment)}),
    HOURS({ int increment, DateTime time -> time.plusHours(increment)}),
    DAYS({ int increment, DateTime time -> time.plusDays(increment)}),
    WEEKS({ int increment, DateTime time -> time.plusWeeks(increment)}),
    MONTHS({ int increment, DateTime time -> time.plusMonths(increment)}),
    YEARS({ int increment, DateTime time -> time.plusYears(increment)})

    Plus(Closure plus) {
        this.plus = plus
    }

    private Closure plus

    Closure getIncrementor(int increment = 1) {
        return plus.curry(increment)
    }
}
