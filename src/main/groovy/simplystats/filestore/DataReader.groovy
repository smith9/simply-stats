package simplystats.filestore

import groovy.json.JsonSlurper
import org.joda.time.DateTime
import org.joda.time.Interval
import simplystats.filestore.criteria.Criteria
import simplystats.store.Data
import simplystats.store.Result
import simplystats.store.Store

/**
 * User: david
 * Date: 27/04/13
 * Time: 9:19 AM
 */
class DataReader {
    FileFinder finder

    DataReader(FileFinder finder) {
        this.finder = finder
    }

    List<Result> read(String measure, Map where, Interval interval, Criteria criteria) {
        Map<String, List<MeasureFile>> filesInInterval = finder.list(interval, measure)
        List<Result> measureLists = []
        filesInInterval.each { String currentMeasure, List<MeasureFile> filesForMeasure ->
            Result result = new Result(currentMeasure)
            result.interval = interval
            result.groupBy = new HashMap(where)
            loadData(result, filesForMeasure, criteria)
            if(!result.isEmpty()) {
                measureLists << result
            }
        }
        return measureLists
    }

    private void loadData(Result result, List<MeasureFile> measureFiles, Criteria criteria) {
        measureFiles.each { MeasureFile measureFile ->
            measureFile.measureFile.eachLine { String line ->
                Map entryMap = readEntry(line)
                Data data = createData(result.measure, entryMap)
                if(criteria.accept(data)) {
                    result << data
                }
            }
        }
    }

    Data createData(String measure, Map dataFileEntry) {
        Data data = new Data()

        data.measure = measure

        String eventDateString = dataFileEntry.remove(Store.FIELD_DATE)
        data.eventTime = new DateTime(eventDateString)

        String durationString = dataFileEntry.remove(Store.FIELD_DURATION)
        data.duration = Double.parseDouble(durationString)

        data.metadata = Collections.unmodifiableMap(dataFileEntry)
        return data;
    }


    Map readEntry(String json) {
        return new JsonSlurper().parseText(json) as Map
    }
}
