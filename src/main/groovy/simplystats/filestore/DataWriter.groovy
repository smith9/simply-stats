package simplystats.filestore

import groovy.json.JsonBuilder
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import org.joda.time.format.ISODateTimeFormat
import simplystats.store.Data

/**
 * User: david
 * Date: 26/04/13
 * Time: 8:42 AM
 */
class DataWriter {
    static final String NEWLINE = System.getProperty("line.separator")
    FileFinder finder

    DataWriter(FileFinder finder) {
        this.finder = finder
    }
    void append(String measure, DateTime periodStart, List<Data> periodData) {
        File measureFile = finder.generateMeasureFile(measure, periodStart)
        ensureParentExists(measureFile)
        measureFile.withWriterAppend { Writer writer ->
            periodData.each { Data data ->
                writer.append(createEntry(data)).append(NEWLINE)
            }
        }
    }

    static void ensureParentExists(File file) {
        File parent = file.parentFile
        if(!parent.exists()) {
            parent.mkdirs()
        }
    }

    String createEntry(Data data) {
        JsonBuilder builder = new JsonBuilder([date:data.eventTime.toString(), duration: data.duration] + data.metadata)
        return builder.toString()
    }
}
