package simplystats.filestore

import org.joda.time.DateTime
import org.joda.time.Interval
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import simplystats.date.DateIterable
import simplystats.date.Plus

/**
 * User: david
 * Date: 25/04/13
 * Time: 5:08 PM
 */
class FileFinder {
    static final DateTimeFormatter YEAR_THRU_MINUTE_FORMAT = DateTimeFormat.forPattern("yyyy/MM/dd/HH/mm".replaceAll("/", File.separator))
    File storeDir

    FileFinder(File storeDir) {
        this.storeDir = storeDir
    }

    Map<String, List<MeasureFile>> list(Interval interval, String measure = null) {
        Map<String, List<MeasureFile>> filesInInterval = listFiles(interval)
        if(measure) {
            return [measure: filesInInterval[measure]]
        } else {
            return filesInInterval
        }
    }

    private Map<String, List<MeasureFile>> listFiles(Interval overallInterval) {
        List<MeasureFile> filesInInterval = []
        new DateIterable(overallInterval, Plus.MINUTES).each { Interval subInterval ->
            File measureDir = generateMeasureDatedDir(subInterval.start)
            if(measureDir.exists()) {
                List<File> measureFiles = measureDir.listFiles().findAll { MeasureFile.isMeasureFile(it) }
                filesInInterval.addAll(measureFiles.collect { new MeasureFile(it)})
            }
        }
        return filesInInterval.groupBy { it.measure }
    }

    File generateMeasureDatedDir(DateTime periodStart) {
        return new File(storeDir, YEAR_THRU_MINUTE_FORMAT.print(periodStart))
    }

    File generateMeasureFile(String measure, DateTime periodStart) {
        File datedDir = generateMeasureDatedDir(periodStart)
        return new File(datedDir, generateMeasureFilename(measure))
    }

    private String generateMeasureFilename(String measure) {
        return "${measure}.${MeasureFile.DATA_EXTENSION}"
    }

}
