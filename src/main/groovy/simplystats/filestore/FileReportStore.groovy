package simplystats.filestore

import org.joda.time.Interval
import simplystats.store.ReportStore

/**
 * User: david
 * Date: 6/05/13
 * Time: 9:07 PM
 */
class FileReportStore implements ReportStore{
    File storeDir

    FileReportStore(File storeDir) {
        this.storeDir = storeDir
    }

    File getReportDir(Set<String> groupBy, Map filter, Interval interval) {
        // TODO how should the store work, do a little design...
        return storeDir
    }
}
