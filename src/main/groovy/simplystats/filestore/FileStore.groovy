package simplystats.filestore

import org.joda.time.DateTime
import org.joda.time.Interval
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import simplystats.date.Period
import simplystats.filestore.criteria.Criteria
import simplystats.filestore.criteria.CriteriaList
import simplystats.filestore.criteria.DurationBetweenCriteria
import simplystats.filestore.criteria.DurationGreaterThanCriteria
import simplystats.filestore.criteria.DurationGreaterThanEqualCriteria
import simplystats.filestore.criteria.DurationLessThanCriteria
import simplystats.filestore.criteria.DurationLessThanEqualCriteria
import simplystats.filestore.criteria.IntervalCriteria
import simplystats.filestore.criteria.WhereCriteria
import simplystats.filter.DataFilter
import simplystats.store.Data
import simplystats.store.Result
import simplystats.store.Store

/**
 * User: david
 * Date: 25/04/13
 * Time: 3:51 PM
 */
class FileStore implements Store {
    static final DateTimeFormatter UUID_PREFIX = DateTimeFormat.forPattern("yyyyMMddHHmm")
    File storeDir
    Period storeSize = Period.Minutes1
    FileFinder finder

    FileStore(File storeDir) {
        this.storeDir = storeDir
        finder = new FileFinder(storeDir)
    }

    void store(List<Data> dataList) {
        List<Result> results = DataFilter.groupByMeasureAndPeriod(storeSize, dataList)
        results.each { Result result ->
            store(result.measure, result.interval.start, result)
        }
    }
    CriteriaList createIntervalWhereCriteria(Map where, Interval interval) {
        CriteriaList criteria = new CriteriaList(
                [new IntervalCriteria(interval),
                 new WhereCriteria(where)])
        return criteria
    }
    List<Result> retrieve(String measure, Map where, Interval interval) {
        return find(measure, where, interval, createIntervalWhereCriteria(where, interval))
    }

    List<Result> retrieveGreaterThan(String measure, double duration, Map where, Interval interval) {
        CriteriaList criteria = createIntervalWhereCriteria(where, interval)
        criteria.criteriaList << new DurationGreaterThanCriteria(duration)
        return find(measure, where, interval, criteria)
    }

    List<Result> retrieveLessThan(String measure, double duration, Map where, Interval interval) {
        CriteriaList criteria = createIntervalWhereCriteria(where, interval)
        criteria.criteriaList << new DurationLessThanCriteria(duration)
        return find(measure, where, interval, criteria)
    }

    List<Result> retrieveGreaterThanEqual(String measure, double duration, Map where, Interval interval) {
        CriteriaList criteria = createIntervalWhereCriteria(where, interval)
        criteria.criteriaList << new DurationGreaterThanEqualCriteria(duration)
        return find(measure, where, interval, criteria)

    }

    List<Result> retrieveLessThanEqual(String measure, double duration, Map where, Interval interval) {
        CriteriaList criteria = createIntervalWhereCriteria(where, interval)
        criteria.criteriaList << new DurationLessThanEqualCriteria(duration)
        return find(measure, where, interval, criteria)
    }

    List<Result> retrieveBetween(String measure, double durationLower, double durationUpper, Map where, Interval interval) {
        CriteriaList criteria = createIntervalWhereCriteria(where, interval)
        criteria.criteriaList << new DurationBetweenCriteria(durationLower, durationUpper)
        return find(measure, where, interval, criteria)
    }

    void store(String measure, DateTime periodStart, List<Data> periodList) {
        new DataWriter(finder).append(measure, periodStart, periodList)
    }

    private List<Result> find(String measure, Map where, Interval interval, Criteria criteria) {
        return new DataReader(finder).read(measure, where, interval, criteria)
    }
}
