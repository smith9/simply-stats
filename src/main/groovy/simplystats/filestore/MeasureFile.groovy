package simplystats.filestore

import org.joda.time.DateTime
import org.joda.time.Interval

/**
 * User: david
 * Date: 27/04/13
 * Time: 4:58 PM
 */
class MeasureFile {
    static final String DATA_EXTENSION = "data"

    File measureFile

    MeasureFile(File measureFile) {
        this.measureFile = measureFile
    }

    static boolean isMeasureFile(File file) {
        return file.name.endsWith(DATA_EXTENSION)

    }

    String getMeasure() {
        return measureFile.name - ".$DATA_EXTENSION"
    }
}
