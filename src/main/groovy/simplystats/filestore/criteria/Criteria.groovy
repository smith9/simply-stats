package simplystats.filestore.criteria

import simplystats.store.Data

/**
 * User: david
 * Date: 11/05/13
 * Time: 3:42 PM
 */
interface Criteria {
    boolean accept(Data data)
}
