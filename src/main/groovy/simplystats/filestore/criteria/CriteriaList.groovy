package simplystats.filestore.criteria

import simplystats.store.Data

/**
 * User: david
 * Date: 11/05/13
 * Time: 5:53 PM
 */
class CriteriaList implements Criteria {
    CriteriaList(List<Criteria> criteriaList) {
        this.criteriaList = criteriaList
    }

    List<Criteria> criteriaList
    boolean accept(Data data) {
        boolean accept = true
        for(Criteria criteria : criteriaList) {
            if(!criteria.accept(data)) {
                accept = false
                break
            }
        }
        return accept
    }
}
