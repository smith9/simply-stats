package simplystats.filestore.criteria

import simplystats.store.Data

/**
 * User: david
 * Date: 11/05/13
 * Time: 5:36 PM
 */
class DurationBetweenCriteria implements Criteria {
    DurationBetweenCriteria(double lower, double upper) {
        this.lower = lower
        this.upper = upper
    }

    double lower
    double upper
    boolean accept(Data data) {
        return data.duration >= lower && data.duration < upper
    }
}
