package simplystats.filestore.criteria

import simplystats.store.Data

/**
 * User: david
 * Date: 11/05/13
 * Time: 5:36 PM
 */
class DurationGreaterThanCriteria implements Criteria {
    DurationGreaterThanCriteria(double duration) {
        this.duration = duration
    }

    double duration
    boolean accept(Data data) {
        return data.duration > duration
    }
}
