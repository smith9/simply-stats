package simplystats.filestore.criteria

import org.joda.time.Interval
import simplystats.store.Data

/**
 * User: david
 * Date: 11/05/13
 * Time: 3:48 PM
 */
class IntervalCriteria implements Criteria {
    IntervalCriteria(Interval interval) {
        this.interval = interval
    }

    Interval interval
    boolean accept(Data data) {
        return interval.contains(data.eventTime)
    }
}
