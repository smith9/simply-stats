package simplystats.filestore.criteria

import simplystats.store.Data

/**
 * User: david
 * Date: 11/05/13
 * Time: 5:33 PM
 */
class WhereCriteria implements Criteria {
    Map<String, String> where

    WhereCriteria(Map<String, String> where) {
        this.where = where
    }

    boolean accept(Data data) {
        boolean criteriaMatch = data.metadata.entrySet().containsAll(where.entrySet())
        return criteriaMatch;
    }
}
