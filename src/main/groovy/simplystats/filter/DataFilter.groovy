package simplystats.filter

import org.joda.time.DateTime
import org.joda.time.Interval
import simplystats.date.Period
import simplystats.store.Data
import simplystats.store.Result

/**
 * User: david
 * Date: 25/04/13
 * Time: 9:45 PM
 */
class DataFilter {
    static List<Result> groupByPeriod(Period period, List<Result> measuresList) {
        List<Result> results = []
        measuresList.each { Result measureList ->
            Map<DateTime, List<Data>> byPeriod = measureList.groupBy { period.floor(it.eventTime) }
            byPeriod.each { DateTime periodStart, List<Data> periodList ->
                Result result = new Result(measureList.measure)
                result.interval = new Interval(periodStart, period.ceiling(periodStart))
                result.dataList = periodList
                results << result
            }
        }
        return results
    }

    static List<Result> groupByMeasure(List<Data> dataList) {
        Map<String, List<Data>> byMeasure = dataList.groupBy { it.measure }
        return byMeasure.collect {
            Result result = new Result((String) it.key)
            result.dataList = it.value
            return result
        }
    }

    static List<Result> groupByMeasureAndPeriod(Period period, List<Data> dataList) {
        List<Result> byMeasure = groupByMeasure(dataList)
        return groupByPeriod(period, byMeasure)
    }

    static List<Result> groupByMetadata(Set metadataKeys, List<Result> dataList) {
        List<Result> results = []
        dataList.each { Result groupThisResult ->
            Map<Map, List<Data>> byMetadata = groupDataByMetadata(groupThisResult, metadataKeys)
            results.addAll(convertToResults(byMetadata, groupThisResult))
        }

        return results
    }

    private static List<Result> convertToResults(Map<Map, List<Data>> byMetadata, Result measurePeriodGroup) {
        return byMetadata.collect {
            Result result = new Result(measurePeriodGroup.measure)
            result.interval = measurePeriodGroup.interval
            result.groupBy = it.key
            result.dataList = it.value
            return result
        }
    }

    private static Map<Map, List<Data>> groupDataByMetadata(Result groupThisResult, Set metadataKeys) {
        Map<Map, List<Data>> byMetadata = groupThisResult.groupBy { Data data ->
            data.metadata.subMap(metadataKeys) }
        return byMetadata
    }
}
