package simplystats.filter

import org.joda.time.Interval
import simplystats.stats.StatsResult

/**
 * User: david
 * Date: 14/05/13
 * Time: 8:35 AM
 */
class StatsFilter {
    static Map<String, List<StatsResult>> groupByMeasure(List<StatsResult> stats) {
        return stats.groupBy { StatsResult statsResult ->
            return statsResult.result.measure
        }
    }
    static Map<Set, List<StatsResult>> groupByMetadata(List<StatsResult> stats) {
        return stats.groupBy { StatsResult statsResult ->
            return statsResult.result.groupBy.keySet()
        }
    }
    static Map<Interval, List<StatsResult>> groupByInterval(List<StatsResult> stats) {
        return stats.groupBy { StatsResult statsResult ->
            return statsResult.result.interval
        }
    }
}
