package simplystats.report

import groovy.text.SimpleTemplateEngine
import groovy.text.Template
import groovy.util.logging.Slf4j
import org.joda.time.Interval
import simplystats.config.Bands
import simplystats.config.Config
import simplystats.filter.DataFilter
import simplystats.stats.BaselineResult
import simplystats.stats.CssCalc
import simplystats.stats.StatsCalculator
import simplystats.stats.StatsResult
import simplystats.store.ReportStore
import simplystats.store.Result
import simplystats.store.StatsStore
import simplystats.store.Store

/**
 * User: david
 * Date: 6/05/13
 * Time: 6:40 PM
 */
@Slf4j
class Reports {
    private Store store
    private StatsStore statsStore
    private ReportStore reportStore
    private Config config

    Reports(ReportStore reportStore, Store store, StatsStore statsStore, Config config) {
        this.store = store
        this.statsStore = statsStore
        this.reportStore = reportStore
        this.config = config
    }

    void generate(String name, Set<String> groupBy, Map<String, String> filterBy, Interval interval) {
        List<Result> results = store.retrieve(null, filterBy, interval)
        List<Result> groupedResults = groupBy.isEmpty() ? results : DataFilter.groupByMetadata(groupBy, results)
        List<StatsResult> statsResults = StatsCalculator.calcDescriptive(groupedResults)
        statsStore.storeStats(null, statsResults)
        List<BaselineResult> baselined = statsStore.getBaseline(statsResults)
        File report = new File(reportStore.getReportDir(groupBy, filterBy, interval), "${name}.html")
        log.info("Creating report ${report.absolutePath}")
        SimpleTemplateEngine engine = new SimpleTemplateEngine()
        String templateText = new File("src/main/resources/templates/stats.gsp").text
        templateText = templateText.replaceAll(/<%@ page import="(.*)" %>/, '<% import $1 %>')
        Template template = engine.createTemplate(templateText)
        Writable result = template.make([stats: baselined, css: config.bands.generateCss(), bands: config.bands, cssCalc: new CssCalc(config.bands)])
        report.text = result.toString()


    }

}
