package simplystats.stats

import simplystats.config.Bands

/**
 * User: david
 * Date: 10/05/13
 * Time: 7:39 PM
 */
class BaselineResult {
    StatsResult baseline
    StatsResult stats
    private BigDecimal diffPercent(BigDecimal statValue, BigDecimal baselineValue) {
        BigDecimal percent = 0
        if (statValue && baselineValue) {
            BigDecimal diff = baselineValue - statValue
            percent = diff / statValue * 100
        }
        return percent
    }
    BigDecimal diffPercentMin() {
        if (baseline) {
            return diffPercent(stats.stats.min, baseline.stats.min)
        }
        return 0
    }

    BigDecimal diffPercentMax() {
        if (baseline) {
            return diffPercent(stats.stats.max, baseline.stats.max)
        }
        return 0
    }

    BigDecimal diffPercentMean() {
        if (baseline) {
            return diffPercent(stats.stats.mean, baseline.stats.mean)
        }
        return 0
    }

    BigDecimal diffPercentGeometricMean() {
        if (baseline) {
            return diffPercent(stats.stats.geometricMean, baseline.stats.geometricMean)
        }
        return 0
    }

    BigDecimal diffPercentStdDev() {
        if (baseline) {
            return diffPercent(stats.stats.stdDev, baseline.stats.stdDev)
        }
        return 0
    }

    BigDecimal diffPercentVariance() {
        if (baseline) {
            return diffPercent(stats.stats.variance, baseline.stats.variance)
        }
        return 0
    }

    BigDecimal diffPercentPercentile(int percentile) {
        if (baseline) {
            return diffPercent(stats.stats.getPercentile(percentile), baseline.stats.getPercentile(percentile))
        }
        return 0
    }

}
