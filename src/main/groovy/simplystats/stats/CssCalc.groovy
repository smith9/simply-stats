package simplystats.stats

import simplystats.config.Bands

/**
 * User: david
 * Date: 23/05/13
 * Time: 11:29 AM
 */
class CssCalc {
    CssCalc(Bands bands) {
        this.bands = bands
    }

    Bands bands
    String meanClass(BaselineResult baselineResult) {
        return bands.getBand(baselineResult.diffPercentMean()).css
    }
    String minClass(BaselineResult baselineResult) {
        return bands.getBand(baselineResult.diffPercentMin()).css
    }
    String maxClass(BaselineResult baselineResult) {
        return bands.getBand(baselineResult.diffPercentMax()).css
    }
    String stdDevClass(BaselineResult baselineResult) {
        return bands.getBand(baselineResult.diffPercentStdDev()).css
    }
    String percentClass(BaselineResult baselineResult, int percentile) {
        return bands.getBand(baselineResult.diffPercentPercentile(percentile)).css
    }
}
