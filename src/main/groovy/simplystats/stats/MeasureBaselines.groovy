package simplystats.stats

/**
 * User: david
 * Date: 15/05/13
 * Time: 10:49 PM
 */
class MeasureBaselines {
    List<StatsResult> baselines

    MeasureBaselines(List<StatsResult> baselines) {
        this.baselines = baselines
    }

    StatsResult match(StatsResult stats) {
        StatsResult bestBaselineMatch = null
        int bestMatchCount = 0
        int exactMatchCount = stats.result.groupBy.size()
        for(StatsResult baseline : baselines) {
            int baselineCount = baseline.result.groupBy.size()
            int matchCount = matchingMetadata(baseline, stats)
            if(matchCount == exactMatchCount == baselineCount) {
                bestBaselineMatch = baseline
                break
            } else if(matchCount > bestMatchCount || bestMatchCount == matchCount && matchCount == baselineCount) {
                bestBaselineMatch = baseline
                bestMatchCount = matchCount
            }
        }
        return bestBaselineMatch
    }

    int matchingMetadata(StatsResult baseline, StatsResult stats) {
        return baseline.result.groupBy.intersect(stats.result.groupBy).size();
    }
}
