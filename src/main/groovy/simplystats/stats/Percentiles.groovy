package simplystats.stats

/**
 * User: david
 * Date: 5/05/13
 * Time: 9:10 PM
 */
class Percentiles {
    Map<Integer, Double> percentiles = [:]

    Double getPercentile(int percentile) {
        percentiles.containsKey(percentile) ? percentiles[percentile] : null
    }

    void setPercentile(int percentile, Double value) {
        percentiles[percentile] = value
    }

    Set<Integer> getPercentileKeys() {
        return percentiles.keySet()
    }
}
