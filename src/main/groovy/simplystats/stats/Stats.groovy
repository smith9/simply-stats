package simplystats.stats

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.apache.commons.math3.stat.descriptive.StatisticalSummary
import org.apache.commons.math3.stat.descriptive.SummaryStatistics

/**
 * User: david
 * Date: 5/05/13
 * Time: 9:02 PM
 */
class Stats {
    Integer count = 0
    BigDecimal min
    BigDecimal max
    BigDecimal mean
    BigDecimal stdDev
    BigDecimal variance
    BigDecimal sum
    BigDecimal geometricMean
    @Delegate
    Percentiles percentiles = new Percentiles()

    Stats(DescriptiveStatistics stats) {
        createStats(stats)
    }

    Stats(SummaryStatistics stats) {
        createStats(stats)
    }

    Stats() {
    }

    void createStats(SummaryStatistics summaryStatistics) {
        extractSummaryStats(summaryStatistics);
    }

    void createStats(DescriptiveStatistics descriptiveStatistics) {
        extractSummaryStats(descriptiveStatistics)
        geometricMean = descriptiveStatistics.geometricMean

        (1..100).each { int percentile ->
            setPercentile(percentile, descriptiveStatistics.getPercentile(percentile))
        }
    }

    void extractSummaryStats(StatisticalSummary statisticalSummary) {
        mean = statisticalSummary.mean
        max = statisticalSummary.max
        min = statisticalSummary.min
        count = statisticalSummary.n
        stdDev = statisticalSummary.standardDeviation
        sum = statisticalSummary.sum
        variance = statisticalSummary.variance
    }

    Map<String, BigDecimal> asMap() {
        return [
                count: count,
                min: min,
                max: max,
                stdDev: stdDev,
                sum: sum,
                variance: variance,
                mean: mean] +
                collectPercentiles()
    }

    Map<String, BigDecimal> collectPercentiles() {
        Map<String, BigDecimal> entries = (1..100).collectEntries { int percentile ->
            ["p_${percentile}": getPercentile(percentile)]
        }
        return entries
    }
}
