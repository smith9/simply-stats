package simplystats.stats

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.apache.commons.math3.stat.descriptive.SummaryStatistics
import simplystats.store.Data
import simplystats.store.Result

/**
 * User: david
 * Date: 2/05/13
 * Time: 9:24 PM
 */
class StatsCalculator {
    static List<StatsResult> calcDescriptive(List<Result> results) {
        return results.collect { Result result ->
            calcDescriptive(result)
        }
    }

    static StatsResult calcDescriptive(Result result) {
        DescriptiveStatistics stats = new DescriptiveStatistics()
        result.each { Data data ->
            stats.addValue(data.duration)
        }
        return new StatsResult(stats, result)
    }
    static List<StatsResult> calcSummary(List<Result> results) {
        return results.collect { Result result ->
            calcSummary(result)
        }
    }

    static StatsResult calcSummary(Result result) {
        SummaryStatistics stats = new SummaryStatistics()
        result.each { Data data ->
            stats.addValue(data.duration)
        }
        return new StatsResult(stats, result)
    }
}
