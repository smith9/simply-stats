package simplystats.stats

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.apache.commons.math3.stat.descriptive.SummaryStatistics
import simplystats.store.Result

/**
 * User: david
 * Date: 5/05/13
 * Time: 10:29 PM
 */
class StatsResult {
    String label
    Stats stats
    Result result

    StatsResult(Stats stats, Result result) {
        this.stats = stats
        this.result = result
    }

    StatsResult(DescriptiveStatistics descriptiveStats, Result result) {
        this.stats = new Stats(descriptiveStats)
        this.result = result
    }

    StatsResult(SummaryStatistics summaryStatistics, Result result) {
        this.stats = new Stats(summaryStatistics)
        this.result = result
    }

}
