package simplystats.statsstore

import org.joda.time.DateTime
import org.joda.time.Interval
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import simplystats.filter.StatsFilter
import simplystats.stats.BaselineResult
import simplystats.stats.MeasureBaselines
import simplystats.stats.StatsResult
import simplystats.store.StatsStore

/**
 * User: david
 * Date: 3/05/13
 * Time: 10:15 PM
 */
class FileStatsStore implements StatsStore {
    static final DateTimeFormatter YEAR_THRU_DAY_FORMAT = DateTimeFormat.forPattern("yyyy/MM/dd".replaceAll("/", File.separator))
    static final DateTimeFormatter INTERVAL_DIR_FORMAT = DateTimeFormat.forPattern("yyyyMMddHHmm")
    File storeDir

    FileStatsStore(File storeDir) {
        this.storeDir = storeDir
    }

    void storeStats(String label, List<StatsResult> results) {
        Map<Interval, List<StatsResult>> grouped = StatsFilter.groupByInterval(results)
        grouped.each { Interval interval, List<StatsResult> byInterval ->
            storeByInterval(interval, byInterval)
        }
    }

    private void storeByInterval(Interval interval, List<StatsResult> statsResults) {
        Map<String, List<StatsResult>> grouped = StatsFilter.groupByMeasure(statsResults)
        grouped.each { String measure, List<StatsResult> byMeasure ->
            storeByMeasure(interval, measure, byMeasure)
        }


    }

    private void storeByMeasure(Interval interval, String measure, List<StatsResult> statsResults) {
        Map<Set, List<StatsResult>> grouped = StatsFilter.groupByMetadata(statsResults)
        grouped.each { Set<String> metadataKeys, List<StatsResult> byMetadata ->
            storeByMetadata(interval, measure, metadataKeys, byMetadata)
        }
    }

    private void storeByMetadata(Interval interval, String measure, Set<String> metadataKeys, List<StatsResult> statsResults) {
        File statsFile = createStatsFilename(measure, interval, metadataKeys)
        new StatsWriter().write(statsFile, statsResults)
    }

    private File createStatsFilename(String measure, Interval interval, Set<String> metadataKeys) {
        File dailyDir = new File(storeDir, interval.start.toString(YEAR_THRU_DAY_FORMAT))
        File measureDir = new File(dailyDir, measure)
        File metadataDir = metadataKeys ? new File(measureDir, metadataKeys.join("-")) : measureDir
        File statsFile = new File(metadataDir, createIntervalFilename(interval))
        return statsFile
    }

    private String createIntervalFilename(Interval interval) {
        return interval.start.toString(INTERVAL_DIR_FORMAT) + "-" + interval.end.toString(INTERVAL_DIR_FORMAT) + ".stats";
    }

    List<StatsResult> retrieve(String measure, Interval interval, Set<String> metadataKeys) {
        if(measure == null) {
            return retrieveAllMeasures(interval, metadataKeys)
        }
        File statsFile = createStatsFilename(measure, interval, metadataKeys)

        if(statsFile.exists()) {
            return new StatsReader().read(statsFile)
        } else {
            return []
        }
    }

    List<StatsResult> retrieveAllMeasures(Interval interval, Set<String> metadataKeys) {
        List<String> measures = determineMeasuresForDay(interval.start)
        List<StatsResult> resultsForAllMeasures = []
        measures.each { String measure ->
            resultsForAllMeasures.addAll(retrieve(measure, interval, metadataKeys))
        }
        return resultsForAllMeasures;
    }

    List<String> determineMeasuresForDay(DateTime periodStart) {
        File dailyDir = new File(storeDir, periodStart.toString(YEAR_THRU_DAY_FORMAT))
        return dailyDir.listFiles().collect{it.name}
    }

    void storeBaseline(List<StatsResult> results) {
        Map<String, List<StatsResult>> grouped = StatsFilter.groupByMeasure(results)
        grouped.each { String measure, List<StatsResult> byMeasure ->
            File baselineFile = createBaselineFilename(measure)
            new StatsWriter().write(baselineFile, byMeasure)
        }
    }
    private File createBaselineFilename(String measure) {
        File baselineDir = new File(storeDir, "baseline")
        return new File(baselineDir, measure + ".base")
    }

    MeasureBaselines getBaseline(String measure) {
        File baselineFile = createBaselineFilename(measure)
        List<StatsResult> baselines = []
        if(baselineFile.exists()) {
            baselines = new StatsReader().read(baselineFile)
        }
        return new MeasureBaselines(baselines)
    }

    List<BaselineResult> getBaseline(List<StatsResult> stats) {
        Map<String, List<StatsResult>> grouped = StatsFilter.groupByMeasure(stats)
        List<BaselineResult> statsAndBaseline = []
        grouped.each { String measure, List<StatsResult> byMeasure ->
            MeasureBaselines measureBaselines = getBaseline(measure)
            statsAndBaseline.addAll(matchBaseline(measureBaselines, byMeasure))
        }
        return statsAndBaseline
    }

    List<BaselineResult> matchBaseline(MeasureBaselines measureBaselines, List<StatsResult> stats) {
        stats.collect { StatsResult stat ->
            new BaselineResult(stats: stat, baseline: measureBaselines.match(stat))
        }
    }
}
