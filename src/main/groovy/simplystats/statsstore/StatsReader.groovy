package simplystats.statsstore

import groovy.json.JsonSlurper
import org.joda.time.DateTime
import org.joda.time.Interval
import simplystats.stats.Stats
import simplystats.stats.StatsResult
import simplystats.store.Result

/**
 * User: david
 * Date: 27/04/13
 * Time: 9:19 AM
 */
class StatsReader {
    List<StatsResult> read(File statsFile) {
        Object json = new JsonSlurper().parse(statsFile.newReader())
        return json.collect { Map stats ->
            createStats(stats)
        }
    }

    StatsResult createStats(Map statsResultJson) {
        Result result = populateResult(statsResultJson)
        Stats stats = populateStats(statsResultJson.stats)
        return new StatsResult(stats, result)
    }

    private Stats populateStats(Map statsJson) {
        // TODO change to only set if file has the value
        Stats stats = new Stats()
        stats.count = statsJson.count
        stats.min = statsJson.min
        stats.max = statsJson.max
        stats.mean = statsJson.mean
        stats.sum = statsJson.sum
        stats.stdDev = statsJson.stdDev
        stats.variance = statsJson.variance
        (1..100).each { int percentile ->
            stats.setPercentile(percentile, statsJson["p_$percentile"])
        }
        return stats
    }

    private Result populateResult(Map statsJson) {
        Result result = new Result(statsJson.measure)
        result.interval = createInterval(statsJson.start, statsJson.end)
        if(statsJson.metadata) {
            result.groupBy = new HashMap(statsJson.metadata)
        }
        return result
    }

    Interval createInterval(String startString, String endString) {
        if(startString && endString) {
            return new Interval(new DateTime(startString), new DateTime(endString))
        } else {
            return null
        }

    }
}
