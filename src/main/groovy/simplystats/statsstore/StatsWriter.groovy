package simplystats.statsstore

import groovy.json.JsonBuilder
import simplystats.stats.StatsResult

/**
 * User: david
 * Date: 12/05/13
 * Time: 1:36 PM
 */
class StatsWriter {
    void write(File statsFile, List<StatsResult> stats) {
        statsFile.parentFile.mkdirs()
        def collection = gatherStats(stats)
        statsFile.withWriter { Writer writer ->
            String prettyJson = new JsonBuilder(collection).toPrettyString()
            writer.write(prettyJson)
        }
    }

    List gatherStats(List<StatsResult> stats) {
        return stats.collect { StatsResult statsResult ->
            gatherStat(statsResult)
        }
    }

    Map gatherStat(StatsResult statsResult) {
        Map result = [
                measure: statsResult.result.measure,
                metadata: statsResult.result.groupBy,
                stats: statsResult.stats.asMap()
        ]
        if(statsResult.result.interval) {
            result["start"] = statsResult.result.interval.start.toString()
            result["end"] = statsResult.result.interval.end.toString()
        }
        return result
    }

}
