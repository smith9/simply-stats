package simplystats.store

import org.joda.time.DateTime

/**
 * User: david
 * Date: 25/04/13
 * Time: 3:36 PM
 */
class Data {
    String measure
    DateTime eventTime
    BigDecimal duration
    Map metadata
}
