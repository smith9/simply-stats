package simplystats.store

/**
 * User: david
 * Date: 11/05/13
 * Time: 4:33 PM
 */
class MeasureList implements List<Data> {
    MeasureList(String measure) {
        this.measure = measure
    }

    String measure
    @Delegate
    List<Data> data = []
}

// List<PeriodList>
// PeriodList: List<Metadata>
