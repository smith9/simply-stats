package simplystats.store

import org.joda.time.Interval

/**
 * User: david
 * Date: 6/05/13
 * Time: 9:04 PM
 */
public interface ReportStore {
    File getReportDir(Set<String> groupBy, Map filter, Interval interval)
}