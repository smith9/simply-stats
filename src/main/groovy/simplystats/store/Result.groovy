package simplystats.store

import org.joda.time.Interval
import simplystats.date.Period
import simplystats.store.Data

/**
 * User: david
 * Date: 1/05/13
 * Time: 12:12 PM
 */
class Result implements List<Data> {
    Result(String measure) {
        this.measure = measure
    }
    Result(String measure, List<Data> data) {
        this.measure = measure
        this.dataList = data
    }

    Interval interval
    Map groupBy = [:]
    String measure
    @Delegate
    List<Data> dataList = []
}
