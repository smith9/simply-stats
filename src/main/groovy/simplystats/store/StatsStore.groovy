package simplystats.store

import org.joda.time.Interval
import simplystats.stats.BaselineResult
import simplystats.stats.MeasureBaselines
import simplystats.stats.StatsResult

/**
 * User: david
 * Date: 3/05/13
 * Time: 9:09 PM
 */
interface StatsStore {
    void storeStats(String label, List<StatsResult> results)
    List<StatsResult> retrieve(String measure, Interval interval, Set<String> metadataKeys)
    void storeBaseline(List<StatsResult> results)
    MeasureBaselines getBaseline(String measure)
    List<BaselineResult> getBaseline(List<StatsResult> stats)
}
