package simplystats.store

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.apache.commons.math3.stat.descriptive.SummaryStatistics
import org.joda.time.DateTime
import org.joda.time.Interval

/**
 * NOTE: when you need separation of data, such as by product or environment, you would create a different store.
 * User: david
 * Date: 25/04/13
 * Time: 3:25 PM
 */
public interface Store {
    static final String FIELD_DURATION = "duration"
    static final String FIELD_DATE = "date"
    static final String FIELD_MEASURE = "measure"

    void store(List<Data> dataList)

    List<Result> retrieve(String measure, Map where, Interval interval)
    List<Result> retrieveGreaterThan(String measure, double duration, Map where, Interval interval)
    List<Result> retrieveLessThan(String measure, double duration, Map where, Interval interval)
    List<Result> retrieveGreaterThanEqual(String measure, double duration, Map where, Interval interval)
    List<Result> retrieveLessThanEqual(String measure, double duration, Map where, Interval interval)
    List<Result> retrieveBetween(String measure, double durationLower, double durationUpper, Map where, Interval interval)
}