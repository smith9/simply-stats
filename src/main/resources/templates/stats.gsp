<%@ page import="simplystats.stats.BaselineResult" %>
<%@ page import="simplystats.stats.StatsResult" %>
<html>
<head>
    <style>
    td {
        padding: 5px;
    }

    table {
        border-collapse: collapse;
    }

    table, th, td {
        border: 1px solid burlywood;
    }

    ${
    css
    }
    </style>
</head>

<body>
<h1>Report</h1>
<table>
    <% BaselineResult first = stats.first()
    StatsResult result = first.stats
    %>
    <tr>
        <th>Measure</th>
        <% if (result) {
            result.result.groupBy.keySet().each {
        %>
        <th>${it}</th>

        <% }
        } %>
        <th>Start</th>
        <th>End</th>
        <th>Count</th>
        <th>Mean</th>
        <th>Min</th>
        <th>25%</th>
        <th>50%</th>
        <th>75%</th>
        <th>90%</th>
        <th>95%</th>
        <th>max</th>
        <th>Std Dev</th>
    </tr>
    <% stats.each { BaselineResult baselined ->
        StatsResult statsResult = baselined.stats
    %>
    <tr>
        <td>${statsResult.result.measure}</td>
    <% if (statsResult) {
        statsResult.result.groupBy.values().each {
    %>
        <td>${it}</td>
    <% }
    } %>
        <td>${statsResult.result.interval.start.toString("dd/MM/yyyy hh:mm")}</td>
        <td>${statsResult.result.interval.end.toString("dd/MM/yyyy hh:mm")}</td>
        <td>${statsResult.stats.count}</td>
        <td class="${cssCalc.meanClass(baselined)}">${String.format("%.2f", statsResult.stats.mean)}</td>
        <td class="${cssCalc.minClass(baselined)}">${String.format("%.2f", statsResult.stats.min)}</td>
        <td class="${cssCalc.percentClass(baselined, 25)}">${String.format("%.2f", statsResult.stats.getPercentile(25))}</td>
        <td class="${cssCalc.percentClass(baselined, 50)}">${String.format("%.2f", statsResult.stats.getPercentile(50))}</td>
        <td class="${cssCalc.percentClass(baselined, 75)}">${String.format("%.2f", statsResult.stats.getPercentile(75))}</td>
        <td class="${cssCalc.percentClass(baselined, 90)}">${String.format("%.2f", statsResult.stats.getPercentile(90))}</td>
        <td class="${cssCalc.percentClass(baselined, 95)}">${String.format("%.2f", statsResult.stats.getPercentile(95))}</td>
        <td class="${cssCalc.maxClass(baselined)}">${String.format("%.2f", statsResult.stats.max)}</td>
        <td class="${cssCalc.stdDevClass(baselined)}">${String.format("%.2f", statsResult.stats.stdDev)}</td>
    </tr>
    <% } %>
</table>

</body>
</html>
