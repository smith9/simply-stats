package simplystats.config

import org.junit.Test
import static org.junit.Assert.*


/**
 * User: david
 * Date: 21/05/13
 * Time: 11:12 PM
 */
class BandListTest {
    @Test
    void testCssGeneration() {
        BandList bands = BandList.load(new File("src/test/resources/config/bands.json"))
        String expectedCss = """.band_1 { background-color: #FF7F91; }
.band_3 { background-color: #86C67C; }
"""
        assertEquals(expectedCss, bands.generateCss())
    }
    @Test
    void testLoad() {
        BandList bands = BandList.load(new File("src/test/resources/config/bands.json"))
        assertNull(bands.get(-10.1))
        checkBand(bands, -9.9 as BigDecimal, -10, "band_1", "#FF7F91")
        checkBand(bands, -0.1 as BigDecimal, -10, "band_1", "#FF7F91")
        checkBand(bands, 0, 0)
        checkBand(bands, 9.9, 0)
        checkBand(bands, 10, 10, "band_3", "#86C67C")
        checkBand(bands, 50, 10, "band_3", "#86C67C")
    }

    void checkBand(BandList bands, BigDecimal checkNumber, BigDecimal expectedThreshold, String expectedCss = null, String expectedColour = null) {
        Band band = bands.get(checkNumber)
        assertEquals(expectedThreshold, band.threshold, 0.00005)
        if(expectedCss) {
            assertEquals(expectedCss, band.css)
        }
        if(expectedColour) {
            assertEquals(expectedColour, band.colour)
        }
    }
}
