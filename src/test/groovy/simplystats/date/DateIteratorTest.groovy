package simplystats.date

import org.joda.time.DateTime
import org.joda.time.Interval
import org.junit.Test

import static org.junit.Assert.*
import static simplystats.date.IteratorEnd.*

/**
 * User: david
 * Date: 27/04/13
 * Time: 10:25 AM
 */
class DateIteratorTest {

    @Test
    void testIncrementMilliByOneNiceRoundRange() {
        [END_AT_RANGE_END, END_ON_OR_AFTER_RANGE_END, END_ON_OR_BEFORE_RANGE_END].each { IteratorEnd currentEndHandling ->
            IteratorExpectations expect = new IteratorExpectations()
            expect.with {
                intervalStart = new DateTime(2012, 12, 31, 23, 59, 59, 999)
                intervalEnd = new DateTime(2013, 1, 1, 0, 0, 0, 001)
                endHandling = currentEndHandling
                increment = 1
                incrementType = Plus.MILLIS
                expectedInterval = [
                        new DateTime(2012, 12, 31, 23, 59, 59, 999),
                        new DateTime(2013, 1, 1, 0, 0, 0, 000),
                        new DateTime(2013, 1, 1, 0, 0, 0, 001),
                ]
            }
            check(expect)
        }
    }

    @Test
    void testIncrementSecondsByOneNiceRoundRange() {
        [END_AT_RANGE_END, END_ON_OR_AFTER_RANGE_END, END_ON_OR_BEFORE_RANGE_END].each { IteratorEnd currentEndHandling ->
            IteratorExpectations expect = new IteratorExpectations()
            expect.with {
                intervalStart = new DateTime(2012, 12, 31, 23, 59, 58)
                intervalEnd = new DateTime(2013, 1, 1, 0, 0, 1)
                endHandling = currentEndHandling
                increment = 1
                incrementType = Plus.SECONDS
                expectedInterval = [
                        new DateTime(2012, 12, 31, 23, 59, 58),
                        new DateTime(2012, 12, 31, 23, 59, 59),
                        new DateTime(2013, 1, 1, 0, 0, 0),
                        new DateTime(2013, 1, 1, 0, 0, 1),
                ]
            }
            check(expect)
        }
    }

    @Test
    void testIncrementMinutesByOneNiceRoundRange() {
        [END_AT_RANGE_END, END_ON_OR_AFTER_RANGE_END, END_ON_OR_BEFORE_RANGE_END].each { IteratorEnd currentEndHandling ->
            IteratorExpectations expect = new IteratorExpectations()
            expect.with {
                intervalStart = new DateTime(2012, 12, 31, 23, 58)
                intervalEnd = new DateTime(2013, 1, 1, 0, 1)
                endHandling = currentEndHandling
                increment = 1
                incrementType = Plus.MINUTES
                expectedInterval = [
                        new DateTime(2012, 12, 31, 23, 58),
                        new DateTime(2012, 12, 31, 23, 59),
                        new DateTime(2013, 1, 1, 0, 0),
                        new DateTime(2013, 1, 1, 0, 1),
                ]
            }
            check(expect)
        }
    }

    @Test
    void testIncrementMinutesByOneAtRangeEnd() {
        IteratorExpectations expect = new IteratorExpectations()
        expect.with {
            intervalStart = new DateTime(2012, 12, 31, 23, 58)
            intervalEnd = new DateTime(2013, 1, 1, 0, 1, 55)
            endHandling = END_AT_RANGE_END
            increment = 1
            incrementType = Plus.MINUTES
            expectedInterval = [
                    new DateTime(2012, 12, 31, 23, 58),
                    new DateTime(2012, 12, 31, 23, 59),
                    new DateTime(2013, 1, 1, 0, 0),
                    new DateTime(2013, 1, 1, 0, 1),
                    new DateTime(2013, 1, 1, 0, 1, 55),
            ]
        }
        check(expect)
    }

    @Test
    void testIncrementMinutesByOneAfterRangeEnd() {
        IteratorExpectations expect = new IteratorExpectations()
        expect.with {
            intervalStart = new DateTime(2012, 12, 31, 23, 58)
            intervalEnd = new DateTime(2013, 1, 1, 0, 1, 55)
            endHandling = END_ON_OR_AFTER_RANGE_END
            increment = 1
            incrementType = Plus.MINUTES
            expectedInterval = [
                    new DateTime(2012, 12, 31, 23, 58),
                    new DateTime(2012, 12, 31, 23, 59),
                    new DateTime(2013, 1, 1, 0, 0),
                    new DateTime(2013, 1, 1, 0, 1),
                    new DateTime(2013, 1, 1, 0, 2),
            ]
        }
        check(expect)
    }

    @Test
    void testIncrementMinutesByOneRangeBeforeRangeEnd() {
        IteratorExpectations expect = new IteratorExpectations()
        expect.with {
            intervalStart = new DateTime(2012, 12, 31, 23, 58)
            intervalEnd = new DateTime(2013, 1, 1, 0, 1, 55)
            endHandling = END_ON_OR_BEFORE_RANGE_END
            increment = 1
            incrementType = Plus.MINUTES
            expectedInterval = [
                    new DateTime(2012, 12, 31, 23, 58),
                    new DateTime(2012, 12, 31, 23, 59),
                    new DateTime(2013, 1, 1, 0, 0),
                    new DateTime(2013, 1, 1, 0, 1),
            ]
        }
        check(expect)
    }

    @Test
    void testIncrementHoursByOneNiceRoundRange() {
        [END_AT_RANGE_END, END_ON_OR_AFTER_RANGE_END, END_ON_OR_BEFORE_RANGE_END].each { IteratorEnd currentEndHandling ->
            IteratorExpectations expect = new IteratorExpectations()
            expect.with {
                intervalStart = new DateTime(2012, 12, 31, 22, 0)
                intervalEnd = new DateTime(2013, 1, 1, 1, 0)
                endHandling = currentEndHandling
                increment = 1
                incrementType = Plus.HOURS
                expectedInterval = [
                        new DateTime(2012, 12, 31, 22, 0),
                        new DateTime(2012, 12, 31, 23, 0),
                        new DateTime(2013, 1, 1, 0, 0),
                        new DateTime(2013, 1, 1, 1, 0),
                ]
            }
            check(expect)
        }
    }

    @Test
    void testIncrementDaysByOneNiceRoundRange() {
        [END_AT_RANGE_END, END_ON_OR_AFTER_RANGE_END, END_ON_OR_BEFORE_RANGE_END].each { IteratorEnd currentEndHandling ->
            IteratorExpectations expect = new IteratorExpectations()
            expect.with {
                intervalStart = new DateTime(2012, 12, 30, 10, 25)
                intervalEnd = new DateTime(2013, 1, 2, 10, 25)
                endHandling = currentEndHandling
                increment = 1
                incrementType = Plus.DAYS
                expectedInterval = [
                        new DateTime(2012, 12, 30, 10, 25),
                        new DateTime(2012, 12, 31, 10, 25),
                        new DateTime(2013, 1, 1, 10, 25),
                        new DateTime(2013, 1, 2, 10, 25),
                ]
            }
            check(expect)
        }
    }

    @Test
    void testIncrementWeeksByOneNiceRoundRange() {
        [END_AT_RANGE_END, END_ON_OR_AFTER_RANGE_END, END_ON_OR_BEFORE_RANGE_END].each { IteratorEnd currentEndHandling ->
            IteratorExpectations expect = new IteratorExpectations()
            expect.with {
                intervalStart = new DateTime(2012, 12, 15, 1, 1)
                intervalEnd = new DateTime(2013, 1, 5, 1, 1)
                endHandling = currentEndHandling
                increment = 1
                incrementType = Plus.WEEKS
                expectedInterval = [
                        new DateTime(2012, 12, 15, 1, 1),
                        new DateTime(2012, 12, 22, 1, 1),
                        new DateTime(2012, 12, 29, 1, 1),
                        new DateTime(2013, 1, 5, 1, 1),
                ]
            }
            check(expect)
        }
    }

    @Test
    void testIncrementMonthsByOneNiceRoundRange() {
        [END_AT_RANGE_END, END_ON_OR_AFTER_RANGE_END, END_ON_OR_BEFORE_RANGE_END].each { IteratorEnd currentEndHandling ->
            IteratorExpectations expect = new IteratorExpectations()
            expect.with {
                intervalStart = new DateTime(2012, 11, 30, 23, 58)
                intervalEnd = new DateTime(2013, 3, 28, 23, 58)
                endHandling = currentEndHandling
                increment = 1
                incrementType = Plus.MONTHS
                expectedInterval = [
                        new DateTime(2012, 11, 30, 23, 58),
                        new DateTime(2012, 12, 30, 23, 58),
                        new DateTime(2013, 1, 30, 23, 58),
                        new DateTime(2013, 2, 28, 23, 58),
                        new DateTime(2013, 3, 28, 23, 58),
                ]
            }
            check(expect)
        }
    }

    @Test
    void testIncrementYearsByOneNiceRoundRange() {
        [END_AT_RANGE_END, END_ON_OR_AFTER_RANGE_END, END_ON_OR_BEFORE_RANGE_END].each { IteratorEnd currentEndHandling ->
            IteratorExpectations expect = new IteratorExpectations()
            expect.with {
                intervalStart = new DateTime(2012, 12, 31, 23, 58)
                intervalEnd = new DateTime(2015, 12, 31, 23, 58)
                endHandling = currentEndHandling
                increment = 1
                incrementType = Plus.YEARS
                expectedInterval = [
                        new DateTime(2012, 12, 31, 23, 58),
                        new DateTime(2013, 12, 31, 23, 58),
                        new DateTime(2014, 12, 31, 23, 58),
                        new DateTime(2015, 12, 31, 23, 58),
                ]
            }
            check(expect)
        }
    }

    @Test
    void testIncrementYearsByOneEndExtendedEndAtRangeEnd() {
        IteratorExpectations expect = new IteratorExpectations()
        expect.with {
            intervalStart = new DateTime(2012, 12, 30, 23, 58)
            intervalEnd = new DateTime(2015, 12, 31, 23, 58)
            endHandling = END_AT_RANGE_END
            increment = 1
            incrementType = Plus.YEARS
            expectedInterval = [
                    new DateTime(2012, 12, 30, 23, 58),
                    new DateTime(2013, 12, 30, 23, 58),
                    new DateTime(2014, 12, 30, 23, 58),
                    new DateTime(2015, 12, 30, 23, 58),
                    new DateTime(2015, 12, 31, 23, 58),
            ]
        }
        check(expect)
    }

    @Test
    void testIncrementYearsByOneEndExtendedEndBeforeRangeEnd() {
        IteratorExpectations expect = new IteratorExpectations()
        expect.with {
            intervalStart = new DateTime(2012, 12, 30, 23, 58)
            intervalEnd = new DateTime(2015, 12, 31, 23, 58)
            endHandling = END_ON_OR_BEFORE_RANGE_END
            increment = 1
            incrementType = Plus.YEARS
            expectedInterval = [
                    new DateTime(2012, 12, 30, 23, 58),
                    new DateTime(2013, 12, 30, 23, 58),
                    new DateTime(2014, 12, 30, 23, 58),
                    new DateTime(2015, 12, 30, 23, 58),
            ]
        }
        check(expect)
    }

    @Test
    void testIncrementYearsByOneEndExtendedEndAfterRangeEnd() {
        IteratorExpectations expect = new IteratorExpectations()
        expect.with {
            intervalStart = new DateTime(2012, 12, 30, 23, 58)
            intervalEnd = new DateTime(2015, 12, 31, 23, 58)
            endHandling = END_ON_OR_AFTER_RANGE_END
            increment = 1
            incrementType = Plus.YEARS
            expectedInterval = [
                    new DateTime(2012, 12, 30, 23, 58),
                    new DateTime(2013, 12, 30, 23, 58),
                    new DateTime(2014, 12, 30, 23, 58),
                    new DateTime(2015, 12, 30, 23, 58),
                    new DateTime(2016, 12, 30, 23, 58),
            ]
        }
        check(expect)
    }

    @Test
    void testIncrementYearsByTwoRangeEnd() {
        IteratorExpectations expect = new IteratorExpectations()
        expect.with {
            intervalStart = new DateTime(2012, 12, 31, 23, 58)
            intervalEnd = new DateTime(2015, 12, 31, 23, 58)
            endHandling = END_AT_RANGE_END
            increment = 2
            incrementType = Plus.YEARS
            expectedInterval = [
                    new DateTime(2012, 12, 31, 23, 58),
                    new DateTime(2014, 12, 31, 23, 58),
                    new DateTime(2015, 12, 31, 23, 58),
            ]
        }
        check(expect)
    }

    @Test
    void testIncrementYearsByTwoAfterRangeEnd() {
        IteratorExpectations expect = new IteratorExpectations()
        expect.with {
            intervalStart = new DateTime(2012, 12, 31, 23, 58)
            intervalEnd = new DateTime(2015, 12, 31, 23, 58)
            endHandling = END_ON_OR_AFTER_RANGE_END
            increment = 2
            incrementType = Plus.YEARS
            expectedInterval = [
                    new DateTime(2012, 12, 31, 23, 58),
                    new DateTime(2014, 12, 31, 23, 58),
                    new DateTime(2016, 12, 31, 23, 58),
            ]
        }
        check(expect)
    }

    @Test
    void testIncrementYearsByTwoBeforeRangeEnd() {
        IteratorExpectations expect = new IteratorExpectations()
        expect.with {
            intervalStart = new DateTime(2012, 12, 31, 23, 58)
            intervalEnd = new DateTime(2015, 12, 31, 23, 58)
            endHandling = END_ON_OR_BEFORE_RANGE_END
            increment = 2
            incrementType = Plus.YEARS
            expectedInterval = [
                    new DateTime(2012, 12, 31, 23, 58),
                    new DateTime(2014, 12, 31, 23, 58),
            ]
        }
        check(expect)
    }

    void check(IteratorExpectations expect) {
        expect.with {
            DateIterable iterable = new DateIterable(new Interval(intervalStart, intervalEnd), incrementType, increment, endHandling)
            Iterator<Interval> iterator = iterable.iterator()
            int numberOfExpectedIteration = expect.expectedInterval.size() - 1
            numberOfExpectedIteration.times { int startIndex ->
                int endIndex = startIndex + 1
                boolean shouldHaveNext = startIndex < (numberOfExpectedIteration - 1)
                checkInterval("processing start index $startIndex, ending $endHandling", expectedInterval[startIndex], expectedInterval[endIndex], iterator, shouldHaveNext)
            }
        }
    }

    void checkInterval(String message, DateTime expectedStart, DateTime expectedEnd, Iterator<Interval> iterator, boolean expectedHasNext) {
        assertEquals(message, new Interval(expectedStart, expectedEnd), iterator.next())
        2.times {
            // check that it doesn't matter how many times this is called we get the same answer
            assertEquals(message + " checking hasNext", expectedHasNext, iterator.hasNext())
        }
    }
}
