package simplystats.date

import org.joda.time.DateTime
import org.joda.time.Interval

/**
 * User: david
 * Date: 1/05/13
 * Time: 5:29 PM
 */
class IteratorExpectations {
    DateTime intervalStart
    DateTime intervalEnd
    int increment
    Plus incrementType
    IteratorEnd endHandling
    List<DateTime> expectedInterval
}
