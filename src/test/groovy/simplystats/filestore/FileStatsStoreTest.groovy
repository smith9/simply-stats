package simplystats.filestore

import org.apache.commons.io.FileUtils
import org.joda.time.DateTime
import org.joda.time.Interval
import org.junit.Before
import org.junit.Test
import simplystats.helpers.StatsHelper
import simplystats.stats.BaselineResult
import simplystats.stats.StatsResult
import simplystats.statsstore.FileStatsStore

import static org.junit.Assert.*
import static simplystats.helpers.StatsHelper.DELTA

/**
 * User: david
 * Date: 4/05/13
 * Time: 10:33 PM
 */
class FileStatsStoreTest {
    private static DateTime START = new DateTime(2013, 5, 4, 23, 30)
    private static DateTime END = new DateTime(2013, 5, 4, 23, 50)
    File storeDir
    FileStatsStore store

    @Before
    void setUp() {
        storeDir = new File("target/FileStatsStoreTest")
        storeDir.mkdirs()
        FileUtils.cleanDirectory(storeDir)
        store = new FileStatsStore(storeDir)

    }

    @Test
    void testStoringAndRetrieveStats() {
        DateTime start = new DateTime(2013, 5, 14, 22, 20)
        ArrayList<StatsResult> statsResults = createStats(start)
        store.storeStats("run 1", statsResults)
        checkStats(1, 5, 10, "End 2 End time", new Interval(start, start.plusMinutes(1)), ["customer", "category"] as Set<String>)
        checkStats(1, 6, 20, "End 2 End time", new Interval(start.plusHours(1), start.plusHours(2)), ["customer", "category"] as Set<String>)
        checkStats(4, 38, 30, "End 2 End time", new Interval(start.plusDays(1), start.plusDays(2)), ["customer", "category"] as Set<String>)
        checkStats(1, 9, 50, "Remote time", new Interval(start.plusDays(1), start.plusDays(2)), ["customer", "category"] as Set<String>)
        checkStats(1, 10, 60, "End 2 End time", new Interval(start.plusDays(1), start.plusDays(2)), ["customer", "category", "type"] as Set<String>)
        checkStats(5, 47, 30, null, new Interval(start.plusDays(1), start.plusDays(2)), ["customer", "category"] as Set<String>)
        assertEquals("check range different to one stored", [], store.retrieve("End 2 End time", new Interval(start, start.plusMinutes(2)), ["customer", "category"] as Set<String>))

    }

    private ArrayList<StatsResult> createStats(DateTime start) {
        List<StatsResult> statsResults = [
                StatsHelper.createResult(5, 10, new Interval(start, start.plusMinutes(1)), [customer: "Customer A", category: "email"], "End 2 End time"),
                StatsHelper.createResult(6, 20, new Interval(start.plusHours(1), start.plusHours(2)), [customer: "Customer A", category: "email"], "End 2 End time"),
                StatsHelper.createResult(7, 30, new Interval(start.plusDays(1), start.plusDays(2)), [customer: "Customer A", category: "email"], "End 2 End time"),
                StatsHelper.createResult(8, 40, new Interval(start.plusDays(1), start.plusDays(2)), [customer: "Customer B", category: "email"], "End 2 End time"),
                StatsHelper.createResult(9, 50, new Interval(start.plusDays(1), start.plusDays(2)), [customer: "Customer B", category: "email"], "Remote time"),
                StatsHelper.createResult(10, 60, new Interval(start.plusDays(1), start.plusDays(2)), [customer: "Customer B", category: "email", type: "online"], "End 2 End time"),
                StatsHelper.createResult(11, 70, new Interval(start.plusDays(1), start.plusDays(2)), [customer: "Customer A", category: "fax"], "End 2 End time"),
                StatsHelper.createResult(12, 80, new Interval(start.plusDays(1), start.plusDays(2)), [customer: "Customer C", category: "fax"], "End 2 End time"),
        ]
        return statsResults
    }

    @Test
    void testRetrieveFromEmptyStore() {
        DateTime start = new DateTime(2013, 5, 14, 22, 20)
        assertEquals([], store.retrieve("some measure", new Interval(start, start.plusMinutes(1)), [] as Set<String>))
    }

    void checkStats(int resultsCount, int statsCount, int minDuration, String measure, Interval interval, Set<String> metadataKeys) {
        List<StatsResult> retrieve = store.retrieve(measure, interval, metadataKeys)
        List<String> messageValues = retrieve.collect {
            it.result.interval.start.toString("dd hh:mm") + "-" +
                    it.result.interval.start.toString("dd hh:mm") + "-" +
                    it.result.groupBy.toString() + "-" +
                    it.result.measure + "-" +
                    it.stats.count
        }
        assertEquals("\n" + messageValues.join(",   \n"), resultsCount, retrieve.size())
        StatsResult first = retrieve.first()
        assertEquals(statsCount, retrieve.sum { it.stats.count })
        assertEquals(minDuration, retrieve.min { it.stats.min }.stats.min, DELTA)
        if (measure) {
            assertEquals(measure, first.result.measure)
        }
        assertEquals(metadataKeys, first.result.groupBy.keySet())
        assertEquals(interval, first.result.interval)
    }

    @Test
    void testBaselineMatch() {
        DateTime start = new DateTime(2013, 5, 14, 22, 20)
        List<StatsResult> baselines = [
                StatsHelper.createBaseline(10, [customer: "Customer A", category: "email"], "End 2 End time"),
                StatsHelper.createBaseline(20, [category: "email"], "End 2 End time"),
                StatsHelper.createBaseline(30, [customer: "Customer A"], "End 2 End time"),
                StatsHelper.createBaseline(40, [customer: "Customer A"], "Remote time"),
                StatsHelper.createBaseline(50, [:], "End 2 End time"),
        ]
        store.storeBaseline(baselines)
        checkBaseLine(10, [customer: "Customer A", category: "email", type:"online"], "End 2 End time")
        checkBaseLine(10, [customer: "Customer A", category: "email"], "End 2 End time")
        checkBaseLine(20, [customer: "Customer B", category: "email"], "End 2 End time")
        checkBaseLine(20, [category: "email"], "End 2 End time")
        checkBaseLine(30, [customer: "Customer A"], "End 2 End time")
        checkBaseLine(30, [customer: "Customer A", category: "fax"], "End 2 End time")
        checkBaseLine(40, [customer: "Customer A"], "Remote time")
        checkBaseLine(50, [something:"somethingA"], "End 2 End time")
        checkBaseLine(50, [:], "End 2 End time")
        checkBaseLine(50, [customer: "Customer B", category: "fax"], "End 2 End time")
        checkNoBaseLine([:], "Remote time")
        checkNoBaseLine([category: "email"], "Remote time")
    }

    void checkBaseLine(int expectedMin, Map<String, String> metadata, String measure) {
        StatsResult result = StatsHelper.createResult(metadata, measure)
        List<BaselineResult> baselineMatch = store.getBaseline([result])
        assertEquals(expectedMin, baselineMatch.first().baseline.stats.min, DELTA)


    }
    void checkNoBaseLine(Map<String, String> metadata, String measure) {
        StatsResult result = StatsHelper.createResult(metadata, measure)
        List<BaselineResult> baselineMatch = store.getBaseline([result])
        assertNull(baselineMatch.first().baseline)


    }
}
