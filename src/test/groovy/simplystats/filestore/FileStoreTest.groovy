package simplystats.filestore

import org.apache.commons.io.FileUtils
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.apache.commons.math3.stat.descriptive.SummaryStatistics
import org.joda.time.DateTime
import org.joda.time.Interval
import org.junit.Before
import org.junit.Test
import simplystats.helpers.DataHelper
import simplystats.helpers.Increment
import simplystats.helpers.ExpectedStats
import simplystats.helpers.StatsHelper
import simplystats.store.Data
import simplystats.store.Result

import static org.junit.Assert.*
import static simplystats.helpers.StatsHelper.DELTA

/**
 * User: david
 * Date: 25/04/13
 * Time: 3:53 PM
 */
class FileStoreTest {
    FileStore store
    File storeDir

    private String MEASURE = "End to End Time"
    private String MEASURE2 = "Part A Time"

    @Before
    void setUp() {
        storeDir = new File("target/FileStoreTest")
        storeDir.mkdirs()
        FileUtils.cleanDirectory(storeDir)
        store = new FileStore(storeDir)

    }

    @Test
    void testStoreAndRetrieveAllDataInTimeRange() {
        ExpectedStats dataListBegin = createSingleTimeRange(1, new DateTime(2013, 4, 26, 17, 32, 0), MEASURE)
        ExpectedStats dataListMiddle = createSingleTimeRange(10, dataListBegin.end.plusMillis(1), MEASURE)
        ExpectedStats dataListEnd = createSingleTimeRange(1, dataListMiddle.end.plusMillis(1), MEASURE)
        store.store(dataListBegin.dataList)
        store.store(dataListMiddle.dataList)
        store.store(dataListEnd.dataList)
        List<Result> retrievedMiddle = store.retrieve(null, [:], dataListMiddle.interval)
        checkDescriptiveStats(dataListMiddle, createDescriptiveStats(retrievedMiddle))
    }
    @Test
    void testStoreAndRetrieveSpecificMeasure() {
        ExpectedStats dataMeasure = createSingleTimeRange(5, new DateTime(2013, 4, 26, 17, 32, 0), MEASURE)
        ExpectedStats dataMeasure2 = createSingleTimeRange(5, dataMeasure.start, MEASURE2)
        store.store(dataMeasure.dataList)
        store.store(dataMeasure2.dataList)
        List<Result> retrievedMeasure = store.retrieve(null, [:], dataMeasure.interval)
        checkDescriptiveStats(dataMeasure, createDescriptiveStats(retrievedMeasure))
    }
    private ExpectedStats createSingleTimeRange(int howMany, DateTime start, String measure, Map map = [group: "groupA", "sub-group":"subGroupA"]) {
        // data that will be in a single file per measure because the increment for howMany < 60 is still within a minute file
        List<Double> durations = StatsHelper.createDurations(howMany, 10, 2.5)
        List<DateTime> times = StatsHelper.createTimes(howMany, start, Increment.Seconds)
        Result dataList = StatsHelper.createData(times, durations, measure, map)
        ExpectedStats expectedStats = new ExpectedStats()
        expectedStats.dataList = dataList
        return expectedStats
    }

    private List<Data> createDataForMetadataTests() {
        List<Data> dataList = []
        dataList << DataHelper.createData(MEASURE, new DateTime(2013, 4, 26, 17, 32, 1), 0.9, [group:"GroupA", category:"CategoryA"])
        dataList << DataHelper.createData(MEASURE, new DateTime(2013, 4, 26, 17, 32, 2), 1, [group:"GroupB", category:"CategoryA"])
        dataList << DataHelper.createData(MEASURE, new DateTime(2013, 4, 26, 17, 32, 3), 1.1, [group:"GroupA", category:"CategoryA"])
        dataList << DataHelper.createData(MEASURE, new DateTime(2013, 4, 26, 17, 32, 4), 1.2, [group:"GroupB", category:"CategoryA"])
        dataList << DataHelper.createData(MEASURE, new DateTime(2013, 4, 26, 17, 33, 1), 1.3, [group:"GroupA", category:"CategoryB"])
        dataList << DataHelper.createData(MEASURE, new DateTime(2013, 4, 26, 17, 33, 2), 1.4, [group:"GroupB", category:"CategoryB"])
        dataList << DataHelper.createData(MEASURE, new DateTime(2013, 4, 26, 17, 33, 3), 1.5, [group:"GroupA", category:"CategoryB"])
        dataList << DataHelper.createData(MEASURE, new DateTime(2013, 4, 26, 17, 33, 4), 1.6, [group:"GroupB", category:"CategoryB"])
        return dataList
    }

    @Test
    void testStoreAndRetrieveSpecificMetadataDifferentFiles() {
        List<Data> dataList = createDataForMetadataTests()
        store.store(dataList)
        // exclusive end of interval so add a milli to include it here
        Interval interval = new Interval(dataList.first().eventTime, dataList.last().eventTime.plusMillis(1))
        List<Result> retrieved = store.retrieve(null, [group:"GroupA"], interval)
        List<Double> durations = extractDurations(retrieved)
        assertArrayEquals([0.9, 1.1, 1.3, 1.5] as double[], durations as double[], DELTA)
    }
    @Test
    void testStoreAndRetrieveSpecificMetadataSameFile() {
        List<Data> dataList = createDataForMetadataTests()
        store.store(dataList)
        // exclusive end of interval so add a milli to include it here
        Interval interval = new Interval(dataList.first().eventTime, dataList.last().eventTime.plusMillis(1))
        List<Result> retrieved = store.retrieve(null, [category:"CategoryA"], interval)
        retrieved.each {
            assertEquals([category:"CategoryA"], it.groupBy)
        }

        List<Double> durations = extractDurations(retrieved)
        assertArrayEquals([0.9, 1.0, 1.1, 1.2] as double[], durations as double[], DELTA)
    }

    List<Double> extractDurations(List<Result> measureLists) {
        List<Double> durations = []
        measureLists.each { Result measureList ->
            durations.addAll(measureList.collect { it.duration })
        }
        return durations
    }

    private List<Data> createDataForDurationTests() {
        List<Data> dataList = []
        dataList << DataHelper.createData(MEASURE, new DateTime(2013, 4, 26, 17, 32, 1), 0.9, [:])
        dataList << DataHelper.createData(MEASURE2, new DateTime(2013, 4, 26, 17, 32, 2), 1, [:])
        dataList << DataHelper.createData(MEASURE, new DateTime(2013, 4, 26, 17, 32, 3), 1.1, [:])
        dataList << DataHelper.createData(MEASURE2, new DateTime(2013, 4, 26, 17, 32, 4), 1.2, [:])
        dataList << DataHelper.createData(MEASURE, new DateTime(2013, 4, 26, 17, 33, 1), 1.3, [:])
        dataList << DataHelper.createData(MEASURE2, new DateTime(2013, 4, 26, 17, 33, 2), 1.4, [:])
        dataList << DataHelper.createData(MEASURE, new DateTime(2013, 4, 26, 17, 33, 3), 1.5, [:])
        dataList << DataHelper.createData(MEASURE2, new DateTime(2013, 4, 26, 17, 33, 4), 1.6, [:])
        return dataList
    }

    @Test
    void testStoreAndRetrieveBetween() {
        List<Data> dataList = createDataForDurationTests()
        store.store(dataList)
        // exclusive end of interval so add a milli to include it here
        Interval interval = new Interval(dataList[1].eventTime, dataList[6].eventTime.plusMillis(1))
        List<Result> retrieved = store.retrieveBetween(null, 1.2, 1.3, [:], interval)
        assertEquals(1, retrieved.size())
        Result measureList = retrieved.first()
        assertEquals(1, measureList.size())
        assertEquals(1.2, measureList.first().duration, DELTA)
    }

    @Test
    void testStoreAndRetrieveLessThanEqual() {
        List<Data> dataList = createDataForDurationTests()
        store.store(dataList)
        // exclusive end of interval so add a milli to include it here
        Interval interval = new Interval(dataList[1].eventTime, dataList[6].eventTime.plusMillis(1))
        List<Result> retrieved = store.retrieveLessThanEqual(null, 1.1, [:], interval)
        assertEquals(2, retrieved.size())
        Result measureList = retrieved[0]
        assertEquals(1, measureList.size())
        assertEquals(1.1, measureList[0].duration, DELTA)
        measureList = retrieved[1]
        assertEquals(1, measureList.size())
        assertEquals(1.0, measureList[0].duration, DELTA)
    }
    @Test
    void testStoreAndRetrieveLessThan() {
        List<Data> dataList = createDataForDurationTests()
        store.store(dataList)
        // exclusive end of interval so add a milli to include it here
        Interval interval = new Interval(dataList[1].eventTime, dataList[6].eventTime.plusMillis(1))
        List<Result> retrieved = store.retrieveLessThan(null, 1.1, [:], interval)
        assertEquals(1, retrieved.size())
        Result measureList = retrieved.first()
        assertEquals(1, measureList.size())
        assertEquals(1.0, measureList.first().duration, DELTA)
    }
    @Test
    void testStoreAndRetrieveGreaterThan() {
        List<Data> dataList = createDataForDurationTests()
        store.store(dataList)
        // exclusive end of interval so add a milli to include it here
        Interval interval = new Interval(dataList[1].eventTime, dataList[6].eventTime.plusMillis(1))
        List<Result> retrieved = store.retrieveGreaterThan(null, 1.4, [:], interval)
        assertEquals(1, retrieved.size())
        Result measureList = retrieved.first()
        assertEquals(1, measureList.size())
        assertEquals(1.5, measureList.first().duration, DELTA)
    }
    @Test
    void testStoreAndRetrieveGreaterThanEqual() {
        List<Data> dataList = createDataForDurationTests()
        store.store(dataList)
        // exclusive end of interval so add a milli to include it here
        Interval interval = new Interval(dataList[1].eventTime, dataList[6].eventTime.plusMillis(1))
        List<Result> retrieved = store.retrieveGreaterThanEqual(null, 1.4, [:], interval)
        assertEquals(2, retrieved.size())
        Result measureList = retrieved[0]
        assertEquals(1, measureList.size())
        assertEquals(1.5, measureList[0].duration, DELTA)
        measureList = retrieved[1]
        assertEquals(1, measureList.size())
        assertEquals(1.4, measureList[0].duration, DELTA)
    }

    ExpectedStats createDescriptiveStats(List<Result> actualData) {
        ExpectedStats actual = new ExpectedStats()
        List<Data> merged = []
        actualData.each { Result measureList ->
            merged.addAll(measureList)
        }
        actual.dataList = merged
        return actual
    }

    void checkDescriptiveStats(ExpectedStats expectedStats, ExpectedStats actual) {
        assertTrue("Expecting to retrieve data", actual.descriptiveStatistics.n > 0)
        checkDescriptiveStats(expectedStats.descriptiveStatistics, expectedStats.descriptiveStatistics)
        // do a spot check of data
        checkData(expectedStats.dataList.first(), actual.dataList.first())
    }

    void checkData(Data expected, Data actual) {
        assertEquals(expected.measure, actual.measure)
        assertEquals(expected.metadata, actual.metadata)
        assertEquals(expected.eventTime, actual.eventTime)
    }

    void checkDescriptiveStats(DescriptiveStatistics expected, DescriptiveStatistics actual) {
        assertEquals(expected.n, actual.n)
        assertEquals(expected.mean, actual.mean, DELTA)
        assertEquals(expected.min, actual.min, DELTA)
        assertEquals(expected.max, actual.max, DELTA)
        assertEquals(expected.getPercentile(90), actual.getPercentile(90), DELTA)
    }
    void checkSummaryStats(SummaryStatistics expected, SummaryStatistics actual) {
        assertEquals(expected.n, actual.n)
        assertEquals(expected.mean, actual.mean, DELTA)
        assertEquals(expected.min, actual.min, DELTA)
        assertEquals(expected.max, actual.max, DELTA)
    }

    @Test
    void testStorageFileStructure() {

        store.store([DataHelper.createData("measureA", new DateTime(2013, 4, 26, 17, 32, 0), 29.5, [Customer:"CustomerA"]),
                DataHelper.createData("measureA", new DateTime(2013, 4, 26, 17, 32, 23), 30.5, [Customer:"CustomerA"]),
                DataHelper.createData("measureA", new DateTime(2013, 4, 26, 17, 33, 0), 31.5, [Customer:"CustomerA"]),
                DataHelper.createData("measureB", new DateTime(2013, 4, 26, 17, 33, 0), 129.5, [Customer:"CustomerA"]),
        ])
        String actualContentMeasureA = new File(storeDir, "2013/04/26/17/32/measureA.data").text
        assertEquals("""{"date":"2013-04-26T17:32:00.000+10:00","duration":29.5,"Customer":"CustomerA"}
{"date":"2013-04-26T17:32:23.000+10:00","duration":30.5,"Customer":"CustomerA"}
""", actualContentMeasureA)
    }

    @Test
    void testRetrieveFromEmptyStore() {
        DateTime start = DateTime.now().minusMinutes(1)
        DateTime end = DateTime.now()
        Interval interval = new Interval(start, end)
        assertTrue(store.retrieve(null, ["Product": "Simply-Stats"], interval).isEmpty())
        assertTrue(store.retrieveGreaterThan(null, 10, ["Product": "Simply-Stats"], interval).isEmpty())
        assertTrue(store.retrieveGreaterThanEqual(null, 10, ["Product": "Simply-Stats"], interval).isEmpty())
        assertTrue(store.retrieveLessThan(null, 10, ["Product": "Simply-Stats"], interval).isEmpty())
        assertTrue(store.retrieveLessThanEqual(null, 10, ["Product": "Simply-Stats"], interval).isEmpty())
        assertTrue(store.retrieveBetween(null, 10, 12, ["Product": "Simply-Stats"], interval).isEmpty())
    }
}
