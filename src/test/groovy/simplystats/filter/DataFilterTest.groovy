package simplystats.filter

import org.joda.time.DateTime
import org.junit.Test
import simplystats.date.Period
import simplystats.helpers.DataHelper
import simplystats.store.Data
import simplystats.store.Result

import static org.junit.Assert.*
import static simplystats.helpers.StatsHelper.DELTA


/**
 * User: david
 * Date: 25/04/13
 * Time: 11:14 PM
 *
 */
class DataFilterTest {
    @Test
    void testGroupByMetadataMultipleKeys() {
        List<Result> input = dataForMetadataTests()
        List<Result> byMetadata = DataFilter.groupByMetadata(["name1", "name2"] as Set<String>, input)

        assertEquals(1, byMetadata[0].size())
        assertEquals([name1:"David", name2:"John"], byMetadata[0].groupBy)
        assertEquals(10, byMetadata[0].first().duration, DELTA)

        assertEquals(1, byMetadata[1].size())
        assertEquals([name1:"David", name2:"Maya"], byMetadata[1].groupBy)
        assertEquals(11, byMetadata[1].first().duration, DELTA)

        assertEquals(2, byMetadata[2].size())
        assertEquals([name1:"Lucy", name2:"Maya"], byMetadata[2].groupBy)
        assertEquals([name1:"Lucy", name2:"Maya"], byMetadata[2].groupBy)
        assertEquals(12, byMetadata[2].get(0).duration, DELTA)
        assertEquals(13, byMetadata[2].get(1).duration, DELTA)
    }
    @Test
    void testGroupByMetadataSingleKey() {
        List<Result> input = dataForMetadataTests()
        List<Result> byMetadata = DataFilter.groupByMetadata(["name1"] as Set<String>, input)

        assertEquals(2, byMetadata[0].size())
        assertEquals([name1:"David"], byMetadata[0].groupBy)
        assertEquals(10, byMetadata[0].get(0).duration, DELTA)
        assertEquals(11, byMetadata[0].get(1).duration, DELTA)

        assertEquals(2, byMetadata[1].size())
        assertEquals([name1:"Lucy"], byMetadata[1].groupBy)
        assertEquals(12, byMetadata[1].get(0).duration, DELTA)
        assertEquals(13, byMetadata[1].get(1).duration, DELTA)
    }

    private List<Result> dataForMetadataTests() {
        List<Data> dataList = []
        dataList << DataHelper.createDataWithMetadata(10, [name1: "David", name2: "John", name3: "Mark"])
        dataList << DataHelper.createDataWithMetadata(11, [name1: "David", name2: "Maya", name3: "Lori"])
        dataList << DataHelper.createDataWithMetadata(12, [name1: "Lucy", name2: "Maya", name3: "David"])
        dataList << DataHelper.createDataWithMetadata(13, [name1: "Lucy", name2: "Maya", name3: "Jack"])
        List<Result> input = DataFilter.groupByMeasure(dataList)
        return input
    }

    @Test
    void testFilterByMeasure() {
        List<Data> dataList = []
        dataList << createData("measureA")
        dataList << createData("measureB")
        dataList << createData("measureA")
        dataList << createData("measureA")
        List<Result> byMeasure = DataFilter.groupByMeasure(dataList)

        assertEquals(3, byMeasure[0].size())
        assertEquals(1, byMeasure[1].size())
    }

    static Data createData(String measure) {
        Data data = new Data()
        data.measure = measure
        data.eventTime = DateTime.now()
        data.duration = Math.random()
        return data
    }
    static Data createData(DateTime when) {
        Data data = new Data()
        data.measure = "some measure"
        data.eventTime = when
        data.duration = Math.random()
        return data
    }

    @Test
    void testFilterByTime() {
        DateTime time1 = new DateTime(2013, 4, 25, 23, 0, 52)
        DateTime timePlus4 = time1.plusMinutes(4)
        DateTime timePlus5 = time1.plusMinutes(5)
        List<Data> dataList = []
        dataList << createData(time1)
        dataList << createData(time1)
        dataList << createData(timePlus4)
        dataList << createData(timePlus5)
        List<Result> byTime = DataFilter.groupByMeasureAndPeriod(Period.Minutes5, dataList)
        assertEquals(2, byTime.size())
        assertEquals(3, byTime[0].size())
        assertEquals(1, byTime[1].size())
    }
}
