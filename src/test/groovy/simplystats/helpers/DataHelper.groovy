package simplystats.helpers

import org.joda.time.DateTime
import simplystats.store.Data

/**
 * User: david
 * Date: 26/04/13
 * Time: 5:49 PM
 */
class DataHelper {
    static Data createDataWithMetadata(double duration, Map metadata) {
        Data data = new Data()
        data.measure = "measure"
        data.eventTime = DateTime.now()
        data.duration = duration
        data.metadata = metadata
        return data
    }
    static Data createDataWithMeasure(String measure) {
        Data data = new Data()
        data.measure = measure
        data.eventTime = DateTime.now()
        data.duration = Math.random()
        return data
    }
    static Data createDataWithTime(String measure, DateTime time) {
        Data data = new Data()
        data.measure = measure
        data.eventTime = time
        data.duration = Math.random()
        return data
    }
    static Data createData(String measure, DateTime eventDate, double duration, Map<String, String> metadata = ["Customer":"CustomerA"]) {
        Data data = new Data()
        data.measure = measure
        data.eventTime = eventDate
        data.duration = duration
        data.metadata = metadata
        return data
    }
    static Data createData(double duration) {
        Data data = new Data()
        data.measure = "measure"
        data.eventTime = DateTime.now()
        data.duration = duration
        return data
    }

}
