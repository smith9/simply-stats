package simplystats.helpers

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.joda.time.DateTime
import org.joda.time.Interval
import simplystats.store.Data

/**
 * User: david
 * Date: 26/04/13
 * Time: 11:34 PM
 */
class ExpectedStats {
    DescriptiveStatistics descriptiveStatistics

    List<Data> getDataList() {
        return dataList
    }

    void setDataList(List<Data> dataList) {
        this.dataList = dataList
        descriptiveStatistics = StatsHelper.calcDescriptive(dataList)
    }

    List<Data> dataList
    Interval getInterval() {
        return new Interval(getStart(), getEnd())
    }
    DateTime getStart() {
        return dataList.first().eventTime
    }
    DateTime getEnd() {
        return dataList.last().eventTime
    }

}
