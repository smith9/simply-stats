package simplystats.helpers

/**
 * User: david
 * Date: 26/04/13
 * Time: 9:38 PM
 */
public enum Increment {
    Milli,
    Seconds,
    Minutes,
    Hours
}