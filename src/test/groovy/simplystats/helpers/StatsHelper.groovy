package simplystats.helpers

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.joda.time.DateTime
import org.joda.time.Interval
import simplystats.stats.Stats
import simplystats.stats.StatsResult
import simplystats.store.Data
import simplystats.store.Result

/**
 * User: david
 * Date: 26/04/13
 * Time: 9:34 PM
 */
class StatsHelper {
    public static final double DELTA = 0.000005
    static List<DateTime> createTimes(int howMany, DateTime start, Increment incrementType) {
        List<DateTime> times = []
        howMany.times { int index ->
            times << getNextDate(index, incrementType, start)
        }
        return times
    }
    static List<Double> createDurations(int howMany, double start, double increment) {
        List<Double> durations = []
        double duration = start
        howMany.times { int index ->
            durations << duration
            duration += increment
        }
        return durations
    }

    static Result createData(List<DateTime> times, List<Double> durations, String measure, Map metadata, Integer startId = null) {
        assert times.size() == durations.size()
        Result dataList = new Result(measure)
        times.eachWithIndex { DateTime time, int index ->
            Data data = new Data()
            data.eventTime = time
            data.duration = durations[index]
            data.measure = measure
            data.metadata = metadata
            if(startId) {
                data.id = startId + index
            }
            dataList.add(data)
        }
        return dataList
    }

    static DateTime getNextDate(int index, Increment incrementType, DateTime startTime) {
        switch (incrementType) {
            case Increment.Milli:
                return startTime.plusMillis(index)
            case Increment.Seconds:
                return startTime.plusSeconds(index)
            case Increment.Minutes:
                return startTime.plusMinutes(index)
            case Increment.Hours:
                return startTime.plusHours(index)
            default:
                throw new IllegalArgumentException("increment type not supported $incrementType")
        }
    }

    static DescriptiveStatistics calcDescriptive(List<Data> dataList) {
        DescriptiveStatistics stats = new DescriptiveStatistics()
        dataList.each { Data data ->
            stats.addValue(data.duration)
        }
        return stats
    }

    static StatsResult createStatsResult(Result result) {
        DescriptiveStatistics stats = calcDescriptive(result)
        StatsResult statsResult = new StatsResult(stats, result)
        return statsResult
    }
    static List<StatsResult> createStatsResults(List<Result> results) {
        return results.collect { Result result ->
            createStatsResult(result)
        }
    }

    static StatsResult createResult(int numberOfStats, double startDuration, Interval interval, Map groupBy, String measure) {
        List<Data> dataList = []
        DateTime start = interval.start
        numberOfStats.times { int index ->
            dataList << DataHelper.createData(measure, start.plusMillis(index), startDuration + index, groupBy)
        }
        Result result = new Result(measure, dataList)
        result.groupBy = groupBy
        result.interval = interval
        return StatsHelper.createStatsResult(result)
    }
    static StatsResult createResult(Map groupBy, String measure) {
        DateTime start = new DateTime(2013, 5, 14, 22, 20)
        return createResult(2, 10, new Interval(start, start.plusMinutes(1)), groupBy, measure)
    }

    static StatsResult createBaseline(Double min, Map<String, String> metadata, String measure) {
        Result result = new Result(measure)
        result.groupBy = metadata
        Stats stats = new Stats()
        stats.min = min
        return new StatsResult(stats, result)
    }
    static StatsResult createBaseline(BigDecimal mean, Double min, Double max, BigDecimal stdDev, Map<Integer, Double> percentiles, Map<String, String> metadata, String measure) {
        Result result = new Result(measure)
        result.groupBy = metadata
        Stats stats = new Stats()
        stats.min = min
        stats.max = max
        stats.mean = mean
        stats.stdDev = stdDev

        percentiles.each { Integer percentile, Double value ->
            stats.setPercentile(percentile, value)
        }
        return new StatsResult(stats, result)
    }
}
