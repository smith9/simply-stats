package simplystats.report

import org.apache.commons.io.FileUtils
import org.joda.time.DateTime
import org.joda.time.Interval
import org.junit.Before
import org.junit.Test
import simplystats.config.Config
import simplystats.date.DateIterable
import simplystats.date.Plus
import simplystats.filestore.FileReportStore
import simplystats.helpers.StatsHelper
import simplystats.statsstore.FileStatsStore
import simplystats.filestore.FileStore
import simplystats.helpers.DataHelper
import simplystats.store.ReportStore
import simplystats.store.StatsStore
import simplystats.store.Store

/**
 * User: david
 * Date: 6/05/13
 * Time: 10:27 PM
 */
class ReportsTest {
    File storeDir
    File statsStoreDir
    File reportStoreDir
    Store store
    StatsStore statsStore
    ReportStore reportStore
    Interval interval
    @Before
    void setUp() {
        storeDir = new File("target/ReportsTest/store")
        storeDir.mkdirs()
        statsStoreDir = new File("target/ReportsTest/stats")
        statsStoreDir.mkdirs()
        reportStoreDir = new File("target/ReportsTest/reports")
        reportStoreDir.mkdirs()
        FileUtils.cleanDirectory(storeDir)
        store = new FileStore(storeDir)
        statsStore = new FileStatsStore(statsStoreDir)
        reportStore = new FileReportStore(reportStoreDir)
        DateTime start = new DateTime(2013, 5, 6, 0, 0)
        interval = new Interval(start, start.plusMinutes(10))
//        interval = new Interval(start, start.plusDays(1))
    }

    @Test
    void testGenerate() {
        addBaseline(statsStore)
        addData(store)
//        List<MeasureList> retrieve = store.retrieve(null, [:], interval)
        File configDir = new File("src/main/resources/config")
        Config config = new Config(configDir)
        new Reports(reportStore, store, statsStore, config).generate("demo-customer", ["customer"] as Set<String>, [:], interval)
        new Reports(reportStore, store, statsStore, config).generate("demo-customer-server", ["customer","server"] as Set<String>, [:], interval)
        new Reports(reportStore, store, statsStore, config).generate("demo-customer-server 0", ["customer","server"] as Set<String>, [server:"Server 0"], interval)
        new Reports(reportStore, store, statsStore, config).generate("demo-server", ["server"] as Set<String>, [:], interval)
    }

    void addBaseline(StatsStore statsStore) {
        statsStore.storeBaseline([StatsHelper.createBaseline(3.5, 0.5, 25, 1, [25: 1, 50: 2, 75: 3, 90: 10, 95: 15] as Map<Integer, Double>, [:], "End to End Time"),
        StatsHelper.createBaseline(3.5, 1, 30, 1, [25: 2, 50: 3, 75: 4, 90: 12, 95: 20] as Map<Integer, Double>, [customer: "Customer A"], "End to End Time")])

    }

    void addData(Store store) {
        DateIterable iterable = new DateIterable(interval, Plus.SECONDS, 5)
        iterable.each { Interval interval ->
            ["Customer A", "Customer B"].each { String customer ->
                double duration = (Math.random()+0.000001) * 10
                double calcDuration = customer == "Customer A" ? duration : duration + 4
                int server = Math.random() * 3
                store.store([DataHelper.createData("End to End Time", interval.start, calcDuration, [customer: customer, server:"Server $server"])])
            }
        }
    }
}
