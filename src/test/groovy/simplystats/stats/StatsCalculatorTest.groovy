package simplystats.stats

import org.junit.Test
import simplystats.helpers.DataHelper
import simplystats.store.Data
import simplystats.store.Result

import static org.junit.Assert.assertEquals
import static simplystats.helpers.StatsHelper.DELTA

/**
 * User: david
 * Date: 2/05/13
 * Time: 9:46 PM
 */
class StatsCalculatorTest {
    @Test
    void testCalcDescriptive() {
        List<StatsResult> stats = StatsCalculator.calcDescriptive(createResults())
        Stats stats1 = stats[0].stats
        Stats stats2 = stats[1].stats
        assertEquals(4, stats1.count)
        assertEquals(10, stats1.min, DELTA)
        assertEquals(40, stats1.max, DELTA)
        assertEquals(25, stats1.mean, DELTA)
        assertEquals(25, stats1.getPercentile(50), DELTA)
        assertEquals(5, stats2.count)
        assertEquals(11, stats2.min, DELTA)
        assertEquals(62, stats2.max, DELTA)
        assertEquals(32.4, stats2.mean, DELTA)
        assertEquals(26.4, stats2.getPercentile(50), DELTA)
    }
    @Test
    void testCalcSummary() {
        List<StatsResult> stats = StatsCalculator.calcSummary(createResults())
        Stats stats1 = stats[0].stats
        Stats stats2 = stats[1].stats
        assertEquals(4, stats1.count)
        assertEquals(10, stats1.min, DELTA)
        assertEquals(40, stats1.max, DELTA)
        assertEquals(25, stats1.mean, DELTA)
        assertEquals(5, stats2.count)
        assertEquals(11, stats2.min, DELTA)
        assertEquals(62, stats2.max, DELTA)
        assertEquals(32.4, stats2.mean, DELTA)
    }
    private List<Result> createResults() {
        return [createResult([10.0, 24.6, 25.4, 40]), createResult([11.0, 23.6, 26.4, 39, 62])]
    }

    private Result createResult(List<Double> durations) {
        List<Data> dataList = durations.collect { DataHelper.createData(it) }
        Result result = new Result("measure")
        result.dataList = dataList
        return result
    }

}
