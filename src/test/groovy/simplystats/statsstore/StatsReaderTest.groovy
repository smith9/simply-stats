package simplystats.statsstore

import org.joda.time.DateTime
import org.joda.time.Interval
import org.junit.Test
import simplystats.helpers.DataHelper
import simplystats.helpers.StatsHelper
import simplystats.stats.Stats
import simplystats.stats.StatsResult
import simplystats.store.Data
import simplystats.store.Result

import static org.junit.Assert.*
import static simplystats.helpers.StatsHelper.DELTA

/**
 * User: david
 * Date: 13/05/13
 * Time: 8:25 AM
 */
class StatsReaderTest {
    private static String MEASURE = "End 2 End Time"
    @Test
    void testReadPartialBaselineFile() {
        File statsFile = new File("src/test/resources/baseline/End 2 End Time.base")
        List<StatsResult> loaded = new StatsReader().read(statsFile)
        assertEquals(3, loaded.size())
        StatsResult firstResult = loaded[0]
        assertEquals("End 2 End Time", firstResult.result.measure)
        assertEquals(25.25, firstResult.stats.getPercentile(25), DELTA)
        assertEquals(99.99, firstResult.stats.getPercentile(99), DELTA)
        assertTrue(firstResult.result.groupBy.isEmpty())
        assertNull(firstResult.result.interval)
        StatsResult secondResult = loaded[1]
        assertEquals([category:"fax", customer:"Customer A"], secondResult.result.groupBy)
    }
    @Test
    void testReadCompleteFile() {
        List<StatsResult> statsResults = [
                createResult(1, [customer:"Customer A", category:"email"], MEASURE),
                createResult(10, [customer:"Customer A", category:"fax"], MEASURE)
        ]
        File statsFile = new File("target/StatsReaderTest.stats")
        new StatsWriter().write(statsFile, statsResults)
        List<StatsResult> loaded = new StatsReader().read(statsFile)
        assertEquals(2, loaded.size())
        checkStatsResults(statsResults, loaded)
    }

    void checkStatsResults(List<StatsResult> expected, List<StatsResult> actual) {
        assertEquals(expected.size(), actual.size())
        expected.size().times { int index ->
            checkStatsResult(expected[index], actual[index])
        }

    }

    void checkStatsResult(StatsResult expected, StatsResult actual) {
        checkResult(expected.result, actual.result)
        checkStats(expected.stats, actual.stats)
    }

    void checkStats(Stats expected, Stats actual) {
        assertTrue(expected.count > 0)
        assertEquals(expected.count, actual.count)
        assertEquals(expected.min, actual.min, DELTA)
        assertEquals(expected.max, actual.max, DELTA)
        assertEquals(expected.mean, actual.mean, DELTA)
        assertEquals(expected.variance, actual.variance, DELTA)
        assertEquals(expected.stdDev, actual.stdDev, DELTA)
        assertEquals(expected.sum, actual.sum, DELTA)
        (1..100).each { int percentile ->
            assertEquals(expected.getPercentile(percentile), actual.getPercentile(percentile), DELTA)
        }
    }

    void checkResult(Result expected, Result actual) {
        assertNotNull(expected.measure)
        assertEquals(expected.measure, actual.measure)
        assertEquals(expected.groupBy, actual.groupBy)
        assertEquals(expected.interval, actual.interval)
    }

    private StatsResult createResult(double startDuration, Map groupBy, String measure) {
        List<Data> dataList = []
        DateTime start = new DateTime(2013, 4, 26, 17, 32, 0)
        100.times { int index ->
            dataList << DataHelper.createData(measure, start.plusMillis(index), startDuration + index, groupBy)
        }
        Result result = new Result(measure, dataList)
        result.groupBy = groupBy
        result.interval = new Interval(dataList.first().eventTime, dataList.last().eventTime.plusMillis(1))
        return StatsHelper.createStatsResult(result)
    }

}
