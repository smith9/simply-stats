package simplystats.statsstore

import org.joda.time.DateTime
import org.joda.time.Interval
import org.junit.Test
import simplystats.helpers.DataHelper
import simplystats.helpers.StatsHelper
import simplystats.stats.StatsResult
import simplystats.store.Data
import simplystats.store.Result

/**
 * User: david
 * Date: 12/05/13
 * Time: 1:51 PM
 */
class StatsWriterTest {
    private static String MEASURE = "End 2 End Time"
    @Test
    void testWrite() {
        List<StatsResult> statsResults = [
                createResult(1, [customer:"Customer A", category:"email"], MEASURE),
                createResult(10, [customer:"Customer A", category:"fax"], MEASURE)
        ]
        new StatsWriter().write(new File("target/StatsWriter.txt"), statsResults)
    }


    private StatsResult createResult(double startDuration, Map groupBy, String measure) {
        List<Data> dataList = []
        dataList << DataHelper.createData(measure, new DateTime(2013, 4, 26, 17, 32, 1), startDuration + 1, groupBy)
        dataList << DataHelper.createData(measure, new DateTime(2013, 4, 26, 17, 32, 2), startDuration + 2, groupBy)
        dataList << DataHelper.createData(measure, new DateTime(2013, 4, 26, 17, 32, 3), startDuration + 3, groupBy)
        dataList << DataHelper.createData(measure, new DateTime(2013, 4, 26, 17, 32, 4), startDuration + 4, groupBy)
        dataList << DataHelper.createData(measure, new DateTime(2013, 4, 26, 17, 33, 1), startDuration + 5, groupBy)
        dataList << DataHelper.createData(measure, new DateTime(2013, 4, 26, 17, 33, 2), startDuration + 6, groupBy)
        dataList << DataHelper.createData(measure, new DateTime(2013, 4, 26, 17, 33, 3), startDuration + 7, groupBy)
        dataList << DataHelper.createData(measure, new DateTime(2013, 4, 26, 17, 33, 4), startDuration + 8, groupBy)
        Result result = new Result(measure, dataList)
        result.groupBy = groupBy
        result.interval = new Interval(dataList.first().eventTime, dataList.last().eventTime.plusMillis(1))
        return StatsHelper.createStatsResult(result)
    }

}
